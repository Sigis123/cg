<!DOCTYPE html>
<html>
<head>
<title>Play event affect selection</title>
</head>
<body>

Select your event affect:
<br><br>


    
    @foreach($eventAffect['eventDescription'] as $key => $description)
        <form action="/play-session/event-management" method="Post">
        @csrf
            <input type="hidden" name={{'affectLevel'}} value={{$key}}>
            Event affect: {{$description}}<br>
            Character stats needed: {{$eventAffect['eventRequirements'][$key]}}<br>
            <input type="submit" value="Choose this effect"><br><br>
        </form>
    @endforeach


</body>
</html>