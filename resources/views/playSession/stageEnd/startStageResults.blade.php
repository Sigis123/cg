<!DOCTYPE html>
<html>
<head>
<title>Get ready for next event selection</title>
</head>
<body>

Character {{$character->name}} stats<br><br>

Main stats:<br>
Health: {{$characterMainStats->health}}<br>
Strength: {{$characterMainStats->strength}}<br>
Momevent: {{$characterMainStats->momevent}}<br>
Intelligence: {{$characterMainStats->intelligence}}<br><br>

Secondary stats:<br>
Attack: {{$characterSecondaryStats->attack}}<br>
Defense: {{$characterSecondaryStats->defense}}<br>
Speed: {{$characterSecondaryStats->speed}}<br>
Smarts: {{$characterSecondaryStats->smarts}}<br><br>

Gold: {{$character->gold}}<br><br>
@if($currentEventNumber == 4 || $currentEventNumber == 8 || $currentEventNumber == 12)
    <form action="/play-session-boss" method="Get">

        <button type="submit" value="Submit">Go the boss event</button><br><br>
    </form>
    <form action="/shop" method="Post">
    @csrf
        <button type="submit" value="Submit">Go the shop</button><br><br>
    </form>
@else
    <form action="/play-session-game-loop/start" method="Get">
        <button type="submit" value="Submit">Go the next event</button><br><br>
    </form>
@endif

<form action="/card-deck" method="Post">
@csrf
    <button type="submit" value="Submit">See left cards</button><br><br>
</form>

</body>
</html>