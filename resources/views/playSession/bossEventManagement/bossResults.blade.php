<!DOCTYPE html>
<html>
<head>
<title>Boss battle results</title>
</head>
<body>

{{$bossEvent->name}} move<br><br>



Your attack outcome:<br>
{{$resultBossDefenseString}}<br><br>

Boss attack outcome:<br>
{{$resultBossAttackString}}<br><br>

<form action="/play-session-boss-loop" method="post">
    @csrf
    <input type="submit" value="Go to next boss move">
</form>
<br


</body>
</html>