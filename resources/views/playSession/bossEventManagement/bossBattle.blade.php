<!DOCTYPE html>
<html>
<head>
<title>Boss battle</title>
</head>
<body>

Defeat {{$bossEvent->name}}<br>
Description: {{$bossEvent->description}}<br>
Health: {{$bossEvent->health}}<br>
Armor: {{$chosenBossArmor}}<br><br>

Boss attack: {{$chosenBossAttack->attack_name}}<br>
Attack description: {{$chosenBossAttack->attack_description}}<br>
Attack: {{$chosenBossAttack->boss_event_affect}}<br><br>

{{$character->name}} stats<br><br>
Health: {{$characterMainStats->health}}<br>
Strength: {{$characterMainStats->strength}}<br>
Momevent: {{$characterMainStats->momevent}}<br>
Intelligence: {{$characterMainStats->intelligence}}<br>
Attack: {{$characterSecondaryStats->attack}}<br>
Defense: {{$characterSecondaryStats->defense}}<br>
Speed: {{$characterSecondaryStats->speed}}<br>
Smarts: {{$characterSecondaryStats->smarts}}<br>
Soul: {{$characterMainStats->soul}}<br>
Items: {{$character->item_id}}<br>
Gold: {{$character->gold}}<br>
 <br><br>

Damage: {{$realStats['damageDealt']}}<br>
Speed level: {{$realStats['dangerLevel']}}<br>
Luck: {{$realStats['luck']}}<br>
Armor: {{$realStats['armor']}}<br>

<br><br>

@if ($errors->any())
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    <br>
@endif

@if(Session::has('error'))
    <li>{{Session::get('error')}}<li>
    <br>
@endif

@if($ammountOfHandCards >= 1)
    Select your cards which you will use<br>
    Your hand<br><br>
    <form action="hand-loop" method="Get">
        @foreach($handCards as $card)
            <input type="radio" name="card" value={{$card->id}}> {{$card->name}}<br>
            {{$card->description}}<br>
        @endforeach
        <br>
        <input type="submit" value="Play a card">
    </form>
    <br>
@endif

<form action="/play-session-boss-results" method="post">
    @csrf
    <input type="submit" value="End your move">
</form>
<br>


@if($characterMainStats->soul >= 2)
    <form action="/play-session/soul-control-cards" method="Get">
        <input type="submit" value="Redraw whole hand"> (requires 2 soul)
    </form>
    <br>
@endif

@if($characterMainStats->soul >= 1)
    <form action="/play-session/soul-control-hand-cards" method="Get">
        @csrf
        @if($ammountOfHandCards == 1)
            <input type="submit" value="Redraw existing cards"> (requires 1 soul)
        @elseif($ammountOfHandCards >= 1)
            <input type="submit" value="Redraw {{$ammountOfHandCards}} cards">  (requires 1 soul)
        @endif
    </form>
    <br>
    <form action="/play-session/soul-control-status-redistribution-from" method="Get">
        <input type="submit" value="Burn 1 soul"> (redistribute one main status to another status, requires 1 soul)
    </form>
    <br>
@endif

@if($characterMainStats->soul >= 2)
    <form action="/play-session/soul-control-status-addition" method="Get">
        <input type="submit" value="Burn 2 souls"> (add 1 to chosen main status, requires 2 soul)
    </form>
@endif

<br>
<form action="/save-game" method="Post">
    @csrf
    <input type="submit" value="Save and quit">
</form>

</body>
</body>
</html>