<!DOCTYPE html>
<html>
<head>
<title>Play session character selection</title>
</head>
<body>

Select your character
<br><br>

@isset($characterNameAmount)
    @foreach($characterNames as $characterName)
       <a href='/play-session/{{$characterName->name}}/deck-choise'>{{$characterName->name}}</a><br>
    @endforeach
@endisset

@empty($characterNameAmount)
    You have not created any decks<br>
    <a href='/card-management/character-card-decs'>Deck creation</a><br>
@endempty

</body>
</html>