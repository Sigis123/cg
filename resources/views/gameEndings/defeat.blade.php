<!DOCTYPE html>
<html>
<head>
<title>defeat screen</title>
</head>
<body>

<h1>You loose</h1>

Event that killed you:
<br>{{$currentEvent->name}}<br><br>
Event reached:<br><br>

Character {{$character->name}} stats:<br><br>

Main stats:<br>
Health: {{$characterMainStats->health}}<br>
Strength: {{$characterMainStats->strength}}<br>
Momevent: {{$characterMainStats->momevent}}<br>
Intelligence: {{$characterMainStats->intelligence}}<br><br>

Secondary stats:<br>
Attack: {{$characterSecondaryStats->attack}}<br>
Defense: {{$characterSecondaryStats->defense}}<br>
Speed: {{$characterSecondaryStats->speed}}<br>
Smarts: {{$characterSecondaryStats->smarts}}<br><br>

Gold: {{$character->gold}}<br><br>

<a href=/menu>Go to main menu</a>

</body>
</html>