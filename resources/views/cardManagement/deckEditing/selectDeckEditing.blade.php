<!DOCTYPE html>
<html>
<head>
<title>character deck editing</title>
</head>
<body>

    {{$characterName}} decks editing
    <br>
    <br>

    @if($decksAmmount > 0)
        Choose a deck:<br>
        ({{$decksAmmount}}/3)<br><br>
        @foreach($decks as $deck)
            <a href='/deck-editing/{{$characterName}}/{{$deck->deck_name}}'>{{$deck->deck_name}}</a>
            <br>   
        @endforeach
    @else
        You have made no decks for this character
    @endif

</body>
</html>
