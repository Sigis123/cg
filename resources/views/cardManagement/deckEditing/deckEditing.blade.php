<!DOCTYPE html>
<html>
<head>
<title>Character deck editing</title>
</head>
<body>

Edit {{$selectedCards['deckName']}} deck<br><br>
Card amount:{{$deckCardAmount}}/{{$characterCardAmount}}

<form action="/deck-editing/{{$characterName}}/{{$selectedCards['deckName']}}" method='post' >
  @csrf
  <br>
  
  Deck name:
  <input type="text" name="deckName"  value="{{$selectedCards['deckName']}}"">
  <input type="hidden" name="originalDeckName"  value={{$originalDeckName}}>
  <br><br>
  
  @foreach($cards as $card)
    @if(isset($selectedCards[$card->id]))
      <input type="checkbox" name={{$card->id}} value="{{$card->name}}" checked>{{$card->name}}<br>
     Card ammount in the deck:
      <select name="{{$card->name}}">
      <option value="{{$selectedCards[$card->name]}}">{{$selectedCards[$card->name]}}</option>
      <option value=1>1</option>
      <option value=2>2</option>
      <option value=3>3</option>
      <option value=4>4</option>
      <option value=5>5</option>  
      <option value=6>6</option>
      <option value=7>7</option>  
      <option value=8>8</option>
      <option value=9>9</option>
      <option value=10>10</option>
      </select><br>
    @else
     <input type="checkbox" name={{$card->id}} value="{{$card->name}}">{{$card->name}}<br>
     Card ammount in the deck:
      <select name="{{$card->name}}">
      <option value=1>1</option>
      <option value=2>2</option>
      <option value=3>3</option>
      <option value=4>4</option>
      <option value=5>5</option>  
      <option value=6>6</option>
      <option value=7>7</option>  
      <option value=8>8</option>
      <option value=9>9</option>
      <option value=10>10</option>
      </select><br>
    @endif
    Description: {{$card->description}}
    <br>
    Status: {{$card->status}}
    <br>
    Value in the deck: {{$card->value_in_the_deck}}
    <br>
    <br>
  @endforeach
  <br>
  <input type="submit" value="Save edited deck">
</form>
<br>

@isset($error)
  {{$error}}
@endisset

</body>
</html>
