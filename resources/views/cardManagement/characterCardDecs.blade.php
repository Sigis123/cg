<!DOCTYPE html>
<html>
<head>
<title>Menu</title>
</head>
<body>

<h1>Choose a character deck</h1>

Create deck:
<br>
@foreach($characters as $character)
    <a href='/deck-creation/{{$character['name']}}'><p>Create a new {{$character['name']}} deck</p></a>
@endforeach
<br>

@isset($editableDeckCharacterName)
    Edit deck:
    <br>
    @foreach($editableDeckCharacterName as $character)
        <a href='/deck-editing/{{$character['name']}}'><p>Edit {{$character['name']}} deck</p></a>
    @endforeach
@endisset

</body>
</html>