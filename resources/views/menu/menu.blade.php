<!DOCTYPE html>
<html>
<head>
<title>Menu</title>
</head>
<body>

<h1>Menu</h1>
<a href="/card-management/character-card-decs"><p>Decks management</p></a>

@if($savedGameCheck)

    <a href="/load-game"><p>Continue last played game</p></a>

@endif

<a href="/play-session/character-choise"><p>Start new play session</p></a>
</body>
</html>