<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBossEventArmorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boss_event_armors', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('boss_event_id');
            $table->string('affect_level_description');
            $table->integer('armor_amount');
            $table->integer('necessary_speed_amount');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boss_event_armors');
    }
}
