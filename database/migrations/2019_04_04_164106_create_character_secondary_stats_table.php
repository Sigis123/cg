<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterSecondaryStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_secondary_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('character_id');
            $table->integer('attack');
            $table->integer('defense');
            $table->integer('speed');
            $table->integer('smarts');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_secondary_stats');
    }
}
