<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBossEventAttacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boss_event_attacks', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('boss_event_id');
            $table->string('attack_name');
            $table->string('attack_description');
            $table->string('boss_event_affect');
            $table->mediumInteger('minimal_random_value');
            $table->mediumInteger('maximum_random_value');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boss_event_attacks');
    }
}
