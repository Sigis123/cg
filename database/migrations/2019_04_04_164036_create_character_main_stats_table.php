<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCharacterMainStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_main_stats', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('character_id');
            $table->integer('health');
            $table->integer('strength');
            $table->integer('momevent');
            $table->integer('intelligence');
            $table->integer('soul');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_main_stats');
    }
}
