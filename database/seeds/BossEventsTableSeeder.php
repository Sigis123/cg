<?php

use Illuminate\Database\Seeder;

class BossEventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('boss_events')->insert([
            [
                'name' => 'Spartakus',
                'description' => 'This is sparda',
                'level' => 2,
                'health' => 50,
                'skip_attack_random_chance' => 10
            ],
        ]);
    }
}
