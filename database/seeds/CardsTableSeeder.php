<?php

use Illuminate\Database\Seeder;

class CardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cards')->insert([
            // Special card
            [
                'name' => 'Horseshoe',
                'value' => 'horseshoe',
                'status' => 'Special card',
                'description' => 'Increases chance to skip an boss attack',
                'value_in_the_deck' => '4',
                'exclusive_character' => '0'
            ],
            [
                'name' => '$$$',
                'value' => '$$$',
                'status' => 'Special card',
                'description' => 'Get 50 gold',
                'value_in_the_deck' => '5',
                'exclusive_character' => '0'
            ],

            // Main stats card

            [
                'name' => 'Steroids',
                'value' => 'steroids',
                'status' => 'Main stats card',
                'description' => 'Add +1 to strength',
                'value_in_the_deck' => '4',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'Health pack',
                'value' => 'healtPack',
                'status' => 'Main stats card',
                'description' => 'Adds +6 to health',
                'value_in_the_deck' => '10',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'Yeezi',
                'value' => 'yeezi',
                'status' => 'Main stats card',
                'description' => 'Add +1 to momevent',
                'value_in_the_deck' => '4',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'NERD',
                'value' => 'nerd',
                'status' => 'Main stats card',
                'description' => 'Add +1 to intelligence',
                'value_in_the_deck' => '4',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'Holy Bible',
                'value' => 'holyBible',
                'status' => 'Main stats card',
                'description' => 'Add +2 to soul',
                'value_in_the_deck' => '4',
                'exclusive_character' => '0'
            ],

            // Secondary stats card

            [
                'name' => 'Strike',
                'value' => 'strike',
                'status' => 'Secondary stats card',
                'description' => 'Add +5 to attack',
                'value_in_the_deck' => '2',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'Royal Guard',
                'value' => 'royalGuard',
                'status' => 'Secondary stats card',
                'description' => 'Add +5 to defense',
                'value_in_the_deck' => '2',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'Sanic',
                'value' => 'sanic',
                'status' => 'Secondary stats card',
                'description' => 'Add +5 to speed',
                'value_in_the_deck' => '2',
                'exclusive_character' => '0'
            ],
            [
                'name' => '200 IQ',
                'value' => '200IQ',
                'status' => 'Secondary stats card',
                'description' => 'Add +5 to smarts',
                'value_in_the_deck' => '2',
                'exclusive_character' => '0'
            ],

            // Mixed stats card

            [
                'name' => 'ME BIG BRAIN',
                'value' => 'meBigBrain',
                'status' => 'Secondary stats card',
                'description' => 'Add +9 to attack, subtract -4 smarts',
                'value_in_the_deck' => '3',
                'exclusive_character' => '0'
            ],
            [
                'name' => 'ME BIGER BRAIN',
                'value' => 'meBigerBrain',
                'status' => 'Secondary stats card',
                'description' => 'Add +9 to attack, subtract -1 intelligence',
                'value_in_the_deck' => '3',
                'exclusive_character' => '0'
            ],
        ]);

    }
}
