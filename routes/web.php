<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('test','TestController@index');

Auth::routes();

Route::get('/menu','MenuController@index');

Route::get(
    '/card-management/character-card-decs',
    'CardManagementController@index'
);

Route::get(
    'deck-creation/{characterName}',
    'DeckCreationController@index'
)->middleware('auth');

Route::post(
    'deck-creation/{characterName}',
    'DeckCreationController@postDeck'
);

Route::get(
    'deck-editing/{characterName}',
    'DeckEditingController@deckSelect'
);

Route::get(
    'deck-editing/{characterName}/{deckName}',
    'DeckEditingController@showDeckEditing'
);

Route::post(
    'deck-editing/{characterName}/{deckName}',
    'DeckEditingController@deckUpdateEditing'
);

Route::get(
    'play-session/character-choise',
    'StartSessionController@characterSelect'
);

Route::get(
    'play-session/{characterName}/deck-choise',
    'StartSessionController@deckSelect'
)->middleware('auth');

Route::get(
    'play-session/{characterName}/{deckName}',
    'StartStageController@sessionGameplay'
)->middleware('auth');

Route::get(
    'hand-loop',
    'HandDrawController@sessionGameplayHandLoop'
);

Route::get(
    'play-session/soul-control-hand-cards',
    'SoulRedrawManagementController@redrawCurrentHandCards'
);

Route::get(
    'play-session/soul-control-cards',
    'SoulRedrawManagementController@redrawWholeHandCards'
);

Route::get(
    'play-session/soul-control-status-redistribution-from',
    'SoulStatusRedistributionManagementController@redistributeStatusFromView'
);

Route::post(
    'play-session/soul-control-status-redistribution-from',
    'SoulStatusRedistributionManagementController@redistributeStatusFrom'
);

Route::get(
    'play-session/soul-control-status-redistribution-to',
    'SoulStatusRedistributionManagementController@redistributeStatusTo'
);

Route::get(
    'play-session/soul-control-status-addition',
    'SoulStatusRedistributionManagementController@statusAdditionView'
);

Route::get(
    'play-session/main-view-after-soul-addition',
    'SoulStatusRedistributionManagementController@statusAddition'
);

Route::post(
    'play-session/event-results',
    'MiddleStageController@eventAffectChoise'
);

Route::post(
    'play-session/event-management',
    'EndStageController@resultStage'
);

Route::get(
    'play-session-game-loop/start',
    'LoopStageController@startStage'
);

Route::get(
    'redirect-event-loop',
    'RedirectEventLoopController@redirectEventLoop'
)->name('redirectEventLoop');

Route::get(
    'redirect-boss-loop',
    'RedirectBossLoopController@redirectBossLoop'
)->name('redirectBossLoop');

Route::get(
    'play-session-boss',
    'SessionBossController@bossView'
);

Route::post(
    'play-session-boss-results',
    'SessionBossResultsController@encouterResults'
);

Route::get(
    'victory-session-boss-results',
    'SessionBossResultsController@victoryResults'
);

Route::post(
    'play-session-boss-loop',
    "SessionBossLoopController@bossView"
);

Route::post(
    'save-game',
    "SessionSaveStateController@saveState"
);

Route::get(
    'load-game',
    'SessionLoadStateController@loadGame'
);





