<?php

namespace App\HelperClasses\DeckManagement;

//use App\Card;
use App\Repositories\CardRepository;


class DeckManagementHelper 
{

    protected $card;

    public function __construct(CardRepository $card) {
        $this->card = $card;
    }


    public function deckCardAmmount(Array $selectedCards) {
        //$selectedCardsValue = $cards->whereIn('name', $selectedCards)->get(['name','value_in_the_deck']);
        $selectedCardsValue = $this->card->findCardsByName($selectedCards);
        $cardDeckAmmount = 0;
        foreach($selectedCardsValue as $selectedCard) {
            $cardDeckAmmount += $selectedCard->value_in_the_deck * $selectedCards[$selectedCard->name];
        }
        return $cardDeckAmmount;
    }

    public function getDeckCardsId(Array $selectedCards) {
        $deckCardsId = '';
        foreach($selectedCards as $cardId => $card) {
            if(is_int($cardId)) {
                $extraString = str_repeat(",$cardId", $selectedCards[$card]);
                $deckCardsId .= $extraString;    
            }
        }
        return $deckCardsId = trim($deckCardsId, ',');
    }

    public function getSavedDeckCards(Deck $deck, User $user, Character $character, Card $cards, $deckName) {

        $deck = $deck->
            where('user_id', $user->id)
            ->where('character_id', $character->id)
            ->where('deck_name', $deckName)
            ->firstOrFail();
        $deckCardsAmmount = array_count_values(explode(',', $deck->deck_cards_id));
        $deckCardsId = array_keys($deckCardsAmmount);
        $deckCards = $cards->whereIn('id', $deckCardsId)->get();

        $selectedCards['deckName'] = $deckName;
        foreach($deckCards as $deckCard) {
            $selectedCards[$deckCard->id] = $deckCard->name;
            $selectedCards[$deckCard->name] = $deckCardsAmmount[$deckCard->id];
        }
        return $selectedCards;
    }
}