<?php

namespace App\HelperClasses\DeckManagement;

use App\HelperClasses\DeckManagement\DeckManagementHelper;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use App\Repositories\CardRepository;
use App\User;
use App\Deck;
use App\Character;
use App\Card;

class DeckManagementCheck
{
    protected $card;
    protected $character;
    protected $deckRepository;
    protected $deckManagementHelper;
    
    public function __construct(
        CardRepository $card,
        CharacterRepository $character,
        DeckRepository $deckRepository,
        DeckManagementHelper $DeckManagementHelper
    ) {
        $this->card = $card;
        $this->character = $character;
        $this->deckRepository = $deckRepository;
        $this->deckManagementHelper = $DeckManagementHelper;
    }
  
    public function deckCardsAmmountCheck(
        Array $selectedCards,
        String $characterName) {
        $permittedCardAmmount = $this->character->getPropertyByKnownProperty(
            'name',
            $characterName,
            'deck_card_ammount');
        $cardDeckAmmount = $this
            ->deckManagementHelperClass
            ->deckCardAmmount($selectedCards);
        if($permittedCardAmmount[0]['deck_card_ammount'] <  $cardDeckAmmount) {
            $extraCardAmmount = $cardDeckAmmount - $permittedCardAmmount;
            if($extraCardAmmount > 0) {
                $extraCardAmmountString = "You have $extraCardAmmount cards, over your deck limit";
            } else {
                $extraCardAmmountString = "You have $extraCardAmmount card, over your deck limit";
            }
            return $extraCardAmmountString;
        }   
    }



    public function databaseDecksAmmountCheck(Character $character, User $user) {
        $databaseDecks= $this->deckRepository
            ->getDeckByChrUsrId(
                $character->id,
                $user->id
        );
        $characterName = $character->name;
        if(count($databaseDecks) >= 3) {
            $deckErrorMessage = "You already have 3 decks with this character";
            return view('/cardManagement/deckCreation/deckCardExcessErrorMessage', compact(
                'deckErrorMessage',
                'characterName'
            ));
        }
    }
    
    public function deckNameCheck(User $user, String $characterName, Array $selectedCards) 
    {
        $deckCheck = $this->deckRepository
            ->findByUserIdCharacterIdAndDeckName(
                $userId, $characterId, $selectedCards
            );
            ->where('user_id', $user->id)
            ->where('character_id', $character->id)
            ->where('deck_name', $selectedCards['deckName'])
            ->first();
            
        if($deckCheck) {
            $deckNameErrorMessage = "This name (".$selectedCards['deckName'].") is already in use";
            throw new \Exception($deckNameErrorMessage);
        }
    }


    public function getDeckCardsId(Array $selectedCards) {
        $deckCardsId = '';
        foreach($selectedCards as $cardId => $card) {
            if(is_int($cardId)) {
                $extraString = str_repeat(",$cardId", $selectedCards[$card]);
                $deckCardsId .= $extraString;    
            }
        }
        return $deckCardsId = trim($deckCardsId, ',');
    }

    public function completeInputedDeckCreationCheck(
        User $user,
        Array $selectedCards,
        $characterName) {
        $extraCardAmmountString = $this->deckCardsAmmountCheck(
            //$this->character,
            //$this->card,
            $selectedCards,
            $characterName
        );
        dd('fixDeckNameErrorMessage');
        $deckNameErrorMessage = $this->deckNameCheck($user, $characterName, $selectedCards);
        $cards = $cards::whereIn('exclusive_character', [$characterName, 0])->get(); 

        if($extraCardAmmountString || $deckNameErrorMessage) {
            return view("cardManagement/deckCreation/characterDeckCreation", compact(
                'characterName',
                'cards',
                'extraCardAmmountString',
                'deckNameErrorMessage',
                'selectedCards'));  
        }
        //$this->databaseDecksAmmountCheck1($decks, $character, $user);
        //$this->deckNameCheck1($decks, $character, $user, $selectedCards);
    }



    public function completeInputedDeckEditionCheck(
        Character $character,
        Deck $decks,
        User $user,
        Card $cards,
        Array $selectedCards,
        $characterName
    ) {

        $extraCardAmmountString = $this->deckCardsAmmountCheck($character, $cards, $selectedCards, $characterName);
        $currentDeckName = $decks->where('id', $selectedCards['deckId'])->get();
        if($selectedCards['deckName'] !== $currentDeckName->deck_name) {
        $deckNameErrorMessage = $this->deckNameCheck($decks, $character, $user, $selectedCards);
        }
        $cards = $cards->whereIn('exclusive_character', [$characterName, 0])->get(); 

        if($extraCardAmmountString || $deckNameErrorMessage) {

            $character = Character::where('name', $characterName)->firstOrFail();
            $cards = $cards::all();
            $deck = $decks::
            where('user_id', $user->id)
            ->where('character_id', $character->id)
            ->where('deck_name', $deckName)
            ->firstOrFail();


            return view("cardManagement/cardEditing/deckEditing", compact(
                'deck',
                'characterName',
                'cards',
                'character',
                'extraCardAmmountString',
                'deckNameErrorMessage',
                'selectedCards'));  
        }


            //'deck',
            //'characterName',
            //'cards',
            //'selectedCards',
            //'character',
            //sita kaad veitku psl'deckCardAmmount'
        //$this->databaseDecksAmmountCheck1($decks, $character, $user);
        //$this->deckNameCheck1($decks, $character, $user, $selectedCards);       
    }




}