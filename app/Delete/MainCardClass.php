<?php

namespace App\Cards;

use App\Characters\CharacterClass;

class MainCardClass
{
    private $character;
	private $cardName;
	private $cardStatus;
	private $cardValueInTheDeck;
	private $cardDescription;
	private $cardExclusiveCharacter;
	
	public function __construct(CharacterClass $character,Array $cardArray) {
		$this->character = $character;
		$this->cardName = (string) $cardArray['name'];
		$this->cardStatus = (string) $cardArray['status'];
		$this->cardValueInTheDeck = (int) $cardArray['value_in_the_deck'];
		$this->cardDescription = (string) $cardArray['description'];
		$this->cardExclusiveCharacter = (string) $cardArray['exclusive_character'];
	}

    public function getCardName() {
        return $this->cardName;
    }

	public function getCardStatus() {
		return $this->cardStatus;
    }

	public function getCardValueInTheDeck() {
		return $this->cardValueInTheDeck;
    }

	public function getCardDescription() {
		return $this->cardDescription;
	}
    
    public function getCardExclusiveCharacter() {
        return $this->cardExclusiveCharacter;
    }

}	