<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Character;

class CharacterMainStats extends Model
{
    use softDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'character_id',
        'health',
        'strength',
        'momevent',
        'intelligence',
        'soul'
    ];
    
    private CONST MAX_AllOWED_HEALTH = 15;
    private CONST MAX_ALLOWED_STRENGTH = 10;
    private CONST MAX_ALLOWED_MOMEVENT = 10;
    private CONST MAX_ALLOWED_INTELLIGENCE = 10;



    public function characterMainStats()
    {
        return $this->hasOne(Character::class);
    }

    public function addHealth(int $amount)
    {   
        $this->health += $amount;
        if($this->health >= self::MAX_AllOWED_HEALTH) {
            return $this->health = self::MAX_AllOWED_HEALTH;
        }
        return $this->health;
    }

    public function subtractHealth(int $amount)
    {
        return $this->health -= $amount;
    }

    public function addStrength(int $amount)
    {
        $this->strength += $amount;
        if($this->strength >= self::MAX_ALLOWED_STRENGTH) {
            return $this->strength = self::MAX_ALLOWED_STRENGTH;
        }
        return $this->strength;
    }

    public function subtractStrength(int $amount)
    {
        return $this->strength -= $amount;
    }

    public function addMomevent(int $amount)
    {
        $this->momevent += $amount;
        if($this->momevent >= self::MAX_ALLOWED_MOMEVENT) {
            return $this->momevent = self::MAX_ALLOWED_MOMEVENT;
        }
        return $this->momevent;
    }

    public function subtractMomevent(int $amount)
    {
        return $this->momevent -= $amount;
    }

    public function addIntelligence(int $amount)
    {
        $this->intelligence += $amount;
        if($this->intelligence >= self::MAX_ALLOWED_INTELLIGENCE) {
            return $this->intelligence = self::MAX_ALLOWED_INTELLIGENCE;
        }
        return $this->intelligence;
    }

    public function subtractIntelligence(int $amount)
    {
        return $this->intelligence -= $amount;
    }

    public function addSoul(int $amount)
    {
        return $this->soul += $amount;
    }

    public function subtractSoul(int $amount)
    {
        return $this->soul -= $amount;
    }

    public function mainStatsArray()
    {
        return $mainStatArray = $this->toArray();
    }
}
