<?php

namespace App\Factories;

use Illuminate\Session\Store;
use App\BossEvents\SpartakusBossEvent;
use App\Exceptions\ClassNonExistingException;

class BossEventsFactory
{

    public function returnBossEventClass(
        string $eventBossName
    ) {

        switch($eventBossName) {

            case 'Spartakus':
                return new SpartakusBossEvent;
            default:
                throw new ClassNonExistingException(
                    "Boss event $eventBossName wasn't found"
                );
        }
    }


}