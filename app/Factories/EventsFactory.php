<?php

namespace App\Factories;

use Illuminate\Session\Store;
use App\Events\CunamusEvent;
use App\Exceptions\ClassNonExistingException;

class EventsFactory
{

    public function returnEventClass(
        Array $gameplaySession,
        string $affectLevel
    ) {
        switch($gameplaySession['eventName']) {

            case 'Cunamus':
                return new CunamusEvent($gameplaySession, $affectLevel);
            default:
                throw new ClassNonExistingException(
                    $gameplaySession['eventName'].
                    "Event ".$gameplaySession['eventName'].
                    " doesn't exist"
                );
        }
            
    }

}