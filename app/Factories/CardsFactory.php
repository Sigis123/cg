<?php

namespace App\Factories;

use App\Cards\MainStatsCards\SteroidsCard;
use App\Cards\SecondaryStatsCards\StrikeCard;
use App\Exceptions\InputDataNonExistingException;
use App\Cards\SpecialCards\CashCashCashCard;
use App\Cards\MainStatsCards\HealthPackCard;
use App\Cards\MainStatsCards\YeeziCard;
use App\Cards\MainStatsCards\NerdCard;
use App\Cards\MainStatsCards\HolyBibleCard;
use App\Cards\SecondaryStatsCards\RoyalGuardCard;
use App\Cards\SecondaryStatsCards\SanicCard;
use App\Cards\SecondaryStatsCards\TwoHundredIQCard;
use App\Cards\MixedStatsCards\MeBigBrainCard;
use App\Cards\MixedStatsCards\MeBigerBrainCard;

class CardsFactory
{
    public function returnCardClass(Array $gameplaySession, string $handCardName) {
        switch($handCardName) {
            case 'Steroids':
                return new SteroidsCard($gameplaySession);
            case 'Strike':
                return new StrikeCard($gameplaySession);
            case '$$$':
                return new CashCashCashCard($gameplaySession);
            case 'Health pack':
                return new HealthPackCard($gameplaySession);
            case 'Yeezi':
                return new YeeziCard($gameplaySession);
            case 'Nerd':
                return new NerdCard($gameplaySession);
            case 'Holy Bible':
                return new HolyBibleCard($gameplaySession);
            case 'Royal Guard':
                return new RoyalGuardCard($gameplaySession);
            case 'Sanic':
                return new SanicCard($gameplaySession);
            case '200 IQ':
                return new TwoHundredIQCard($gameplaySession);
            case 'ME BIG BRAIN':
                return new MeBigBrainCard($gameplaySession);
            case 'ME BIGER BRAIN':
                return new MeBigerBrainCard($gameplaySession);
            default:
                throw new InputDataNonExistingException(
                    "We couldn't find and play card because we couldn't find "
                    .$handCardName.' card class'
                );
        }
    }
}