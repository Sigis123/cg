<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    
    use softDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'event_affect_id',
        'name',
        'description',
        'level',
        'level_description',
        'event_main_status_to_destroy_name',
        'event_main_status_to_destroy_value'
    ];
}
