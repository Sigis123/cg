<?php

namespace App\BossEvents;

use App\Exceptions\MethodNonExistingException;

class SpartakusBossEvent
{

    public function bossAction(Array $gameplaySession)
    {
        $attackName = $gameplaySession['chosenBossAttack']->attack_name;
        
        switch($attackName) {

            case 'sword':
                return $this->sword($gameplaySession);
            case 'spear':
                return $this->spear($gameplaySession);
            default:
                throw new MethodNonExistingException(
                    "Boss attack $attackName wasn't found"
                );
        }
    }

    public function sword(Array $gameplaySession): string
    {
        /*
        $characterMainStats = $this->session->get('characterMainStats');
        $characterSecondaryStats = $this->session->get('characterSecondaryStats');
        $chosenBossAttack = $this->session->get('chosenBossAttack');
        $attackAmount = $chosenBossAttack->boss_event_affect;
        */
        $characterArmor = 
            $gameplaySession['realStats']['armor'];  

        $bossAttackAmount = 
            $gameplaySession['chosenBossAttack']->boss_event_affect;

        if($characterArmor < $bossAttackAmount) {

            $subtractHealth = $bossAttackAmount - $characterArmor;

            $gameplaySession['characterMainStats']
                ->subtractHealth($subtractHealth);

            return "You lost $subtractHealth health";    
        }

        $lostArmorAmount = 
            $characterArmor['realStats']['armor'] - $bossAttackAmount;

        return "You lost $lostArmorAmount armor";
    }


    public function spear(Array $gameplaySession): string
    {
        /*
        $characterMainStats = $this->session->get('characterMainStats');
        $characterSecondaryStats = $this->session->get('characterSecondaryStats');
        $chosenBossAttack = $this->session->get('chosenBossAttack');
        $attackAmount = $chosenBossAttack->boss_event_affect;
        */
        $characterArmor = 
            $gameplaySession['realStats']['armor'];  
        
        $bossAttackAmount = 
            $gameplaySession['chosenBossAttack']->boss_event_affect;

        if($characterArmor < $bossAttackAmount) {

            $subtractHealth = $bossAttackAmount - $characterArmor;

            $gameplaySession['characterMainStats']
                ->subtractHealth($subtractHealth);

            return "You lost $subtractHealth health";    
        }

        $lostArmorAmount = 
            $characterArmor['realStats']['armor'] - $bossAttackAmount;

        return "You lost $lostArmorAmount armor";
    }   

}
