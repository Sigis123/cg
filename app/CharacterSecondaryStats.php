<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Character;

class CharacterSecondaryStats extends Model
{
    use softDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'character_id',
        'attack',
        'defense',
        'speed',
        'smarts'
    ];

    public function characterSecondaryStats()
    {
        return $this->hasOne(Character::class);
    }

    public function addAttack(int $ammount)
    {
        return $this->attack += $ammount;
    }

    public function subtractAttack(int $ammount)
    {
        return $this->attack -= $ammount;
    }

    public function addDefense(int $ammount)
    {
        return $this->defense += $ammount;
    }

    public function subtractDefense(int $ammount)
    {
        return $this->defense -= $ammount;
    }

    public function addSpeed(int $ammount)
    {
        return $this->speed += $ammount;
    }

    public function subtractSpeed(int $ammount)
    {
        return $this->speed -= $ammount;
    }

    public function addSmarts(int $ammount)
    {
        return $this->health += $ammount;
    }

    public function subtractSmarts(int $ammount)
    {
        return $this->health -= $ammount;
    }
}
