<?php

namespace App\Characters;

class CharacterClass
{
	private $characterName;
	private $characterHealth;
	private $characterStrenght;
	private $characterMomevent;
	private $characterIntelligence;
	private $characterAttack;
	private $characterDefense;
	private $characterSpeed;
	private $characterSmarts;
	private $characterSoul;
	private $characterItem;
	private $characterGold;
	private $characterCardAmmount;

	public 	function __construct(Array $character)
	{	
		$this->characterName = (string) $character['name'];
		$this->characterHealth = (int) $character['health'];
		$this->characterStrenght = (int) $character['strenght'];
		$this->characterMomevent = (int) $character['momevent'];
		$this->characterIntelligence = (int) $character['intelligence'];
		$this->characterAttack = (int) $character['attack'];
		$this->characterDefense = (int) $character['defense'];
		$this->characterSpeed = (int) $character['speed'];
		$this->characterSmarts = (int) $character['smarts'];
		$this->characterSoul = (int) $character['soul'];
		$this->characterItem = (string) $character['item_id'];
		$this->characterGold = (int) $character['gold'];
		$this->characterCardAmmount = (int) $character['deck_card_ammount'];
	}
	
	public function getCharacterName()
	{
		return $this->characterName;
	}

	public function getCharacterHealth()
	{
		return $this->characterHealth;
	}

	public function addCharacterHealth(int $externalHealth)
	{
		return $this->characterHealth += $externalHealth;
	}

	public function deductCharacterHealth(int $externalHealth)
	{
		return $this->characterHealth -= $externalHealth;
	}

	public function getCharacterStrenght()
	{
		return $this->characterStrenght;
	}

	public function addCharacterStrenght(int $externalStrenght)
	{
		return $this->characterStrenght += $externalStrenght; 
	}

	public function deductCharacterStrenght(int $externalStrenght)
	{
		return $this->characterStrenght -= $externalStrenght;
	}

	public function getCharacterMomevent()
	{
		return $this->characterMomevent;
	}

	public function getCharacterIntelligence()
	{
		return $this->characterIntelligence;
	}

	public function getCharacterAttack()
	{
		return $this->characterAttack; 
	}

	public function getCharacterDefense()
	{
		return $this->characterDefense;
	}

	public function getCharacterSpeed()
	{
		return $this->characterSpeed;
	}

	public function getCharacterSmarts()
	{
		return $this->characterSmarts;
	}

	public function getCharacterSoul()
	{
		return $this->characterSoul;
	}

	public function getCharacterItem()
	{
		return $this->characterItem;
	}

	public function getCharacterGold()
	{
		return $this->characterGold;
	}

	public function addCharacterGold(int $externalGold)
	{
		$this->characterGold += $externalGold;
	}

	public function deductCharacterGold(int $externalGold)
	{
		$this->characterGold -= $externalGold;
	}

	public function getCharacterCardAmmount()
	{
		return $this->characterCardAmmount;
	}




	public function setCharacterStrenght(int $extraStrenghtAmmount)
	{
		return $this->character['strenght'] += $extraStrenghtAmmount;
	}

	public function setCharacterMomevent(int $extraMovementAmmount)
	{
		return $this->character['momevent'] += $extraMovementAmmount;
	}

	public function setCharacterHealth(int $additionalHealth)
	{
		return ($this->character['health'] += $additionalHealth);
	}

	

}