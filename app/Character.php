<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\CharacterMainStats;
use App\CharacterSecondaryStats;
use App\User;

class Character extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'main_stats_id',
        'secondary_stats_id',
        'item_id',
        'gold',
        'deck_card_ammount'
    ];

    public function user() {
        $this->belongsTo(User::class);
    }

    public function mainStats()
    {
        return $this->hasOne(CharacterMainStats::class);
    }

    public function secondaryStats()
    {
        return $this->hasOne(CharacterSecondaryStats::class);
    }

    public function addGold(int $ammount)
    {
        return $this->gold += $ammount;
    }

    public function subtractGold(int $ammount)
    {
        return $this->gold -= $ammount;
    }

}
