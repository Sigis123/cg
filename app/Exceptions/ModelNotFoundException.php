<?php

namespace App\Exceptions;

use Exception;

class ModelNotFoundException extends Exception
{
    public function report(Exception $exception) {

        return parent::render($exception);
        //\Log::debug('errorMessage');
    }    
}
