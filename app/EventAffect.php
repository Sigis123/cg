<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventAffect extends Model
{
    use softDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'event_id',
        'level_of_affect',
        'affect_level_description',
        'affect_description',
        'attack',
        'event_main_status_to_destroy_name',
        'event_main_status_to_destroy_value'
    ];  
}
