<?php

namespace App\Repositories;

use App\CharacterMainStats;
use App\Exceptions\RepositoryResponseNotFoundException;

class CharacterMainStatsRepository
{
    public function findByCharacterId(int $characterId)
    {
        $characterMainStats = CharacterMainStats::where('character_id', $characterId)->select(
            'health',
            'strength',
            'momevent',
            'intelligence',
            'soul'
            )->first();

        if(!$characterMainStats || empty($characterMainStats)) {
           throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving character main 
                stats from repository, by character id '.$characterId
            );
        }

        return $characterMainStats;
    }
}