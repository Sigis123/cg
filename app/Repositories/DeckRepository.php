<?php

namespace App\Repositories;

use App\Deck;
use App\Exceptions\RepositoryResponseNotFoundException;
//use App\Repositories\Eloquent\Repository;

class DeckRepository
{

    public function all() 
    {
        return Deck::all();
    }

    public function findNamesWithoutExclusiveNames (int $userId, int $characterId, Array $deckNames)
    {
        $deck = Deck::
            whereNotIn('deck_name', $deckNames)
            ->where('user_id', $userId)
            ->where('character_id', $characterId)
            ->get(['deck_name']);
        
        if(!$deck) {
            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving deck from repository, 
                for excisting deck names'
            );
        }

        return $deck;        
    }

    public function findByCharacterId (int $characterId)
    {
        $deck = Deck::where('character_id', $characterId)->get();

        if(!$deck || $deck->isEmpty()) {

            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving deck from repository, 
                by character id '.$characterId
            ); 
        }

        return $deck;
    }

    public function findByCharacterIdUserId($characterId, $userId) 
    {
        $deck = Deck::
            where('character_id', $characterId)
            ->where('user_id', $userId)
            ->get();

        if(!$deck) {
            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving deck from repository, 
                by character id '.$characterId.' and by user id '.$userId
            );
        }

        return $deck;
    }

    public function findByUserIdCharacterIdDeckName(int $userId, int $characterId, string $deckName)
    {   
        $deck = Deck::
            where('user_id', $userId)
            ->where('character_id', $characterId)
            ->where('deck_name', $deckName)
            ->first();

        if(!$deck || empty($deck)) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving deck from repository, 
                by character id '.$characterId.' ,deck name '.$deckName
            );
        }

        return $deck;
    }

    public function getByUserIdCharacterIdAndDeckName(int $userId, int $characterId, string $deckName)
    {
        $deck = Deck::
            where('user_id', $userId)
            ->where('character_id', $characterId)
            ->where('deck_name', $deckName)
            ->get();
        
        if(!$deck) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving deck from repository, 
                by character id '.$characterId.' and $deck name '.$deckName
            );
        }

        return $deck;
    }

    public function findCharacterIdByUserId(int $userId)
    {
        $characterId = Deck::where('user_id', $userId)->get(['character_id']);

        if(!$characterId) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving deck from repository, 
                by user id '.$userId
            );
        }

        return $characterId;
    }
}