<?php

namespace App\Repositories;

use App\CharacterSecondaryStats;
use App\Exceptions\RepositoryResponseNotFoundException;

class CharacterSecondaryStatsRepository
{
    public function findByCharacterId(int $characterId)
    {
        $characterSecondaryStats = CharacterSecondaryStats::
            where('character_id', $characterId)->first();
        
        if(!$characterSecondaryStats || empty($characterSecondaryStats)) {
           throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving character secondary 
                stats from repository, by character id '.$characterId
            );
        }

        return $characterSecondaryStats;
    }
}