<?php

namespace App\Repositories;

use App\BossEvent;
use App\Exceptions\RepositoryResponseNotFoundException;

class BossEventRepository
{
    public function findByLevel($level)
    {
        $bossEvent = BossEvent::where('level', $level)->get();
        
        if(!$bossEvent || $bossEvent->isEmpty()) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving boss event from repository, 
                by character event level '.$level
            );
        }
        
        return $bossEvent;
    }
}