<?php

namespace App\Repositories;

use App\BossEventArmor;
use App\Exceptions\RepositoryResponseNotFoundException;

class BossEventArmorRepository
{
    public function findByBossId($bossId)
    {
        $bossEventArmor = BossEventArmor::where('boss_event_id', $bossId)->get();

        if(!$bossEventArmor || $bossEventArmor->isEmpty())
        {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving boss armor from repository, 
                by boss event id '.$bossId
            );
        }

        return $bossEventArmor;
    }
}