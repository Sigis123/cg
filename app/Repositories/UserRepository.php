<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    public function findByCharactersExclusivity(Array $characters) {
       return User::whereIn('exclusive_character', $characters);
    }
}