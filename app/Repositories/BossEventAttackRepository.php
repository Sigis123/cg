<?php

namespace App\Repositories;

use App\BossEventAttack;
use App\Exceptions\RepositoryResponseNotFoundException;

class BossEventAttackRepository
{
    public function findByBossId(int $bossId)
    {
        $bossEventAttack = BossEventAttack::
            where('boss_event_id', $bossId)
            ->get();
        
        if(!$bossEventAttack || $bossEventAttack->isEmpty()) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving boss attack from repository, 
                by boss event id '.$bossId
            );
        }

        return $bossEventAttack;
    }
}