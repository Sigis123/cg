<?php

namespace App\Repositories;

use App\GameSave;
use App\Exceptions\RepositoryResponseNotFoundException;
//use App\Repositories\Eloquent\Repository;

class GameSaveRepository
{

    public function getByUserId(int $userId) 
    {
        $gameSave = GameSave::where('user_id', $userId)->get();

        if(!$gameSave || empty($gameSave)) {

            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving character from repository'
            );
        }

        return $gameSave;
    }

    public function getWithDeleteByUserId(int $userId) 
    {
        $gameSave = GameSave::withTrashed()->where('user_id', $userId)->get();

        if(!$gameSave || empty($gameSave)) {

            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving character from repository'
            );
        }

        return $gameSave;
    }
}