<?php

namespace App\Repositories;

use App\EventAffect;
use App\Event;
use App\Exceptions\RepositoryResponseNotFoundException;

class EventRepository
{

    public function eventAffects()
    {
        return $this->hasMany(EventAffect::class);
    }

    public function findById(Int $id)
    {
        return Event::where('id', $id)->get();
    }

    public function findIdsByEventLevel(Int $level)
    {
        return Event::where('level', $level)->pluck('id')->toArray();
    }

    public function findByEventLevel(int $level)
    {
        $event = Event::where('level', $level)->get();

        if(!$event) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving event from repository, 
                by event level '
            );
        }

        return $event;
    }
}