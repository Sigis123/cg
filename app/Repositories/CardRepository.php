<?php

namespace App\Repositories;

use App\Card;
use App\Exceptions\RepositoryResponseNotFoundException;
//use App\Repositories\Eloquent\Repository;


class CardRepository
{

    public function all() {
        $card = Card::all();
        
        if(!$card || $card->isEmpty()) {
            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving cards from repository'
            );
        }

        return $card;
    }

    public function findByCharactersExclusivity(Array $characters) 
    {
       $card = Card::whereIn('exclusive_character', $characters)->get();

       if(!$card || $card->isEmpty()) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving cards from repository, 
                by exclusivity for '.$characters
           );
       }
       
       return $card;
    }

    public function findByCardsId($deckCardsId)
    {
        $cards = Card::whereIn('id', $deckCardsId)->get();

        if(!$cards || $cards->isEmpty()) {
            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving card from repository, 
                by id '.$deckCardsId
            );
        }

        return $cards;
    }

    public function findCardsByName(Array $selectedCards) 
    {
        $cards = Card::whereIn('name', $selectedCards)->get();

        if(!$cards || $cards->isEmpty()) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving card from repository, 
                by selected cards '.$selectedCards
            );
        }

        return $cards;
    }
}