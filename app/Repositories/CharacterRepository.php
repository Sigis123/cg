<?php

namespace App\Repositories;

use App\Character;
use App\Exceptions\RepositoryResponseNotFoundException;
//use App\Repositories\Eloquent\Repository;


class CharacterRepository
{

    public function getName() 
    {
        $character = Character::get(['name']);

        if(!$character || empty($character)) {

            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving character from repository'
            );
        }

        return $character;
    }

    public function findByName($characterName) 
    {
        $character = Character::where('name', $characterName)->first();

        if(!$character || empty($character)) {

            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving character from repository, 
                by character name '.$characterName
            );
        }

        return $character;
    }

    public function findAllIdNames() 
    {
        $characterNames = Character::pluck('name', 'id');

        if(!$characterNames) {
            throw new RepositoryResponseNotFoundException (
                'Something went wrong with retrieving character from repository'
            );
        }

        return $characterNames;
    }
    
    public function getDeckCardAmmountById($id)
    {
        $deckCardAmmount = Character::where('id', $id)->get(['deck_card_ammount']);

        if(!$deckCardAmmount || $deckCardAmmount->isEmpty()){
            
            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving character from repository, 
                by character id '.$id
            );
        }

        return $deckCardAmmount;
    }

    public function findNameById($characterId)
    {
        $characterName = Character::whereIn('id', $characterId)->get();

        if(!$characterName) {

            throw new RepositoryResponseNotFoundException(
                'Something went wrong with retrieving character from repository, 
                by character id '.$characterId
            );
        }

        return $characterName;
    }

    /*
    public function getPropertyByKnownProperty(
        String $knownName,
        String $knownValue,
        String $propertyName){
        return Character::where($knownName, $knownValue)->get([$propertyName]);
    }
    */

}