<?php

namespace App\Repositories;

use App\EventAffect;
use App\Event;
use App\Exceptions\RepositoryResponseNotFoundException;

class EventAffectRepository
{
    public function events()
    {
        return $this->belongsTo(Event::class);
    }

    public function findByEventId(int $eventId)
    {
        $eventAffect = EventAffect::where('event_id', $eventId)->get();

        if(!$eventAffect || $eventAffect->isEmpty()) {

            throw new RepositoryResponseNotFoundException(
                "Something went wrong with retrieving event affect 
                from repository, by it's event id ".$eventId
            );
        }

        return $eventAffect;
    }

}