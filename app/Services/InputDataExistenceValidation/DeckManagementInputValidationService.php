<?php

namespace App\Services\InputDataExistenceValidation;

use App\Exceptions\InputDataNonExistingException;

class DeckManagementInputValidationService
{
    public function deckNameExistenceValidation(Array $deck) 
    {
        
        if(!$deck['deckName'])
        {
            throw new InputDataNonExistingException(
                'You have to input name of the deck'
            );
        }
    }

    public function cardExistenceValidation(Array $deck)
    {
        foreach($deck as $key => $card) {
            if(is_int($key)) {
                $cardExistence = 1;
                break;
            }
        }

        if(!isset($cardExistence)) {
            throw new InputDataNonExistingException(
                'You have to choose at least one card'
            );
        }
    }
}