<?php

namespace App\Services\CardManagement;

use App\Factories\CardsFactory;
use App\Exceptions\InputDataNonExistingException;

class CardClassRetrievementService
{
    /** @var CardsFactory **/
    protected $cardsFactory;

    public function __construct(
        CardsFactory $cardsFactory
    ) {
        $this->cardsFactory = $cardsFactory;
    }

    public function gettingCardClass(
        Array $gameplaySession,
        int $playedCardId
    ) {
        foreach($gameplaySession['handCards'] as $handCard) {
        
            if($handCard->id === $playedCardId) {

                $cardClass = $this->cardsFactory->returnCardClass(
                    $gameplaySession,
                    $handCard->name
                );

                return $cardClass;
            } 
        }

        throw new InputDataNonExistingException(
            "We couldn't find and play your chosen card with id "
            .$playedCardId
        );
    }
}