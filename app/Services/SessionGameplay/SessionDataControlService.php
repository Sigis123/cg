<?php

namespace App\Services\SessionGameplay;

use Illuminate\Database\Eloquent\Collection;
use App\Event;

class SessionDataControlService
{
    public function removeUsedCardFromHand(Array $handCards, int $playedCardId)
    {
        foreach($handCards as $handKey => $handCard) {

                if($handCard->id == $playedCardId) {

                    unset($handCards[$handKey]);
                    break;
                }
            }
            
        return $handCards;
    }

    public function removeUsedCardFromDeck(Array $deckCardsId, int $playedCardId)
    {
        foreach($deckCardsId as $handCardIdKey => $handCardId) {

            if($handCardId == $playedCardId) {

                unset($deckCardsId[$handCardIdKey]);
                break;
            }
        }

        return $deckCardsId;
    }

    public function removeEventFromEventPool(
        Collection $eventPool,
        Event $eventToRemove
    ) {
        foreach($eventPool as $key => $event) {

            if($event == $eventToRemove)

                $eventPool->forget($key);
                break;
        }

        return $eventPool;
    }    
}
