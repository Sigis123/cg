<?php

namespace App\Services\SessionGameplay;

use App\Services\SaveLoadManagement\GameSaveDeletionService;

class GameplayEndService 
{
    private CONST STAT_LIMIT_FOR_LOSING = 0;

    /** @var GameSaveDeletionService **/
    protected $gameSaveDeletionService;

    public function __construct(
        GameSaveDeletionService $gameSaveDeletionService
    ) {
        $this->gameSaveDeletionService = $gameSaveDeletionService;
    }

    public function defeatedByEventCheck(
        int $userId,
        Array $gameplaySession,
        int $minimalMainStatValue    
    ) {
        if($minimalMainStatValue <= self::STAT_LIMIT_FOR_LOSING) {

            $this->gameSaveDeletionService->gameEndingSaveDeletion($userId);

            return view('gameEndings/defeat', [
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'currentEvent' => $gameplaySession['event']
            ]);
        }
    }
}