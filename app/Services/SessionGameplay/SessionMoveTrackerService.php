<?php

namespace App\Services\SessionGameplay;

use App\Character;
use App\CharacterMainStats;
use App\CharacterSecondaryStats;
use App\Event;
use App\EventAffect;
use App\BossEvent;
use App\BossEventAttack;

class SessionMoveTrackerService
{

    public function gamePlayStartTracker(
        string $characterName,
        string $deckName,
        Event $currentEvent,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats,
        string $deckIdString,
        Array $handCards

    ) 
    {

        $moveArray = [[
            'moveName' => 'gamePlayStart',
            'characterName' => $characterName,
            'deckName' => $deckName,
            'currentEvent' => $currentEvent,
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats,
            'deckIdString' => $deckIdString,
            'handCards' => $handCards
        ]];

        return $moveTracker[] = $moveArray;

        /*
        return $moveTracker = [
            'gamePlayStart',
            $characterName,
            $deckName,
            $currentEvent,
            $character,
            $characterMainStats,
            $characterSecondaryStats,
            $deckIdString,
            $handCards
        ];
        */
    }

    public function handLoopTracker(
        Array $moveTracker,
        string $playedCardId,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats,
        Array $handCards
    )
    {
        $moveArray = [ 
            'moveName' => 'PlayedCard',
            'playedCardId' => $playedCardId,
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats,
            'handCards' => $handCards
        ];

        array_push($moveTracker, $moveArray);

        return $moveTracker;

        /*
        array_push(
            $moveTracker,
            'PlayedCard',
            $playedCardId,
            $character,
            $characterMainStats,
            $characterSecondaryStats,
            $handCards
        );
        */
    }

    public function chosenEventTracker(
        Array $moveTracker,
        EventAffect $chosenEventAffect,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    )
    {

        $moveArray = [
            'moveName' => 'chosenEventAffect',
            'chosenEventAffect' => $chosenEventAffect,
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats
        ];

        array_push($moveTracker, $moveArray); 

        return $moveTracker;
    }

    public function gamePlayLoopTracker(
        Array $moveTracker,
        Array $deckCardsId,
        Event $currentEvent,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    )
    {

        $moveArray = [
            'moveName' => 'loopStage',
            'deckCardsId' => $deckCardsId,
            'currentEvent' => $currentEvent,
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats
        ];

        array_push($moveTracker, $moveArray);

        return $moveTracker;    
    }

    public function redrawHandTracker(
        Array $moveTracker,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats,
        Array $handCards
    )
    {

        $moveArray = [
            'moveName' => 'redrawHandCards',
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats,
            'handCards' => $handCards
        ];

        array_push($moveTracker, $moveArray);

        return $moveTracker;
    }

    public function statusRedistributionTracker(
        Array $moveTracker,
        CharacterMainStats $characterMainStats,
        Array $realStats
    )
    {

        $moveArray = [
            'moveName' => 'redrawHandCards',
            'characterMainStats' => $characterMainStats,
            'realStats' => $realStats
        ];

        array_push($moveTracker, $moveArray);

        return $moveTracker;
    }

    public function bossEventTracker(
        Array $moveTracker,
        BossEvent $bossEvent,
        BossEventAttack $chosenBossAttack,
        Array $deckCardsId,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    )
    {
        $moveArray = [
            'moveName' => 'bossSession',
            'bossEvent' => $bossEvent,
            'chosenBossAttack' => $chosenBossAttack,
            'deckCardsId' => $deckCardsId,
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats
        ]; 

        array_push($moveTracker, $moveArray);

        return $moveTracker;
    }

    public function bossWinTracker(
        Array $moveTracker,
        Character $character,
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats

    )
    {

        $moveArray = [
            'moveName' => 'bossWinTracker',
            'character' => $character,
            'characterMainStats' => $characterMainStats,
            'characterSecondaryStats' => $characterSecondaryStats
        ];

        array_push($moveTracker, $moveArray);

        return $moveTracker;
    }
}