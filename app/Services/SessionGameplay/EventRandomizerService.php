<?php

namespace App\Services\SessionGameplay;

use App\Repositories\EventRepository;
use Illuminate\Database\Eloquent\Collection;
use App\Exceptions\DataNonExistingException;

class EventRandomizerService
{
    /** @var EventRepository **/
    protected $eventRepository;

    public function __construct(
        EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;        
    }

    public function levelOfEvents(int $currentEventNumber = 1)
    {

        if($currentEventNumber <= 3) {

            return $levelOfEvent = 1;
        } elseif($currentEventNumber == 4) {

            return $levelOfEvent = 2;
        } elseif($currentEventNumber <= 7) {

            return $levelOfEvent = 3;
        } elseif($currentEventNumber == 8) {

            return $levelOfEvent = 4;
        } elseif($currentEventNumber <= 11) {

            return $levelOfEvent = 5;
        } elseif($currentEventNumber == 12) {
            
            return $levelOfEvent = 6;
        }
        /*
        switch($currentEventNumber) {
            case 1:
                return $levelOfEvent = 1;
            case 4:
                return $levelOfEvent = 2;
            case 5:
                return $levelOfEvent = 3;
            case 8:
                return $levelOfEvent = 4;
            case 9:
                return $levelOfEvent = 5;
            case 12:
                return $levelOfEvent = 6;
            default:
                return $levelOfEvent;
        }
        */
    }


    public function eventRandomizer(Collection $events)
    {
        $currentEvent = $events->random();

            if(!$currentEvent) {

                throw new DataNonExistingException(
                    "Event couldn't be randomized, event collection is empty"
                );
            }
            
        return $currentEvent;
    }
}