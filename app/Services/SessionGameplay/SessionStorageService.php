<?php

namespace App\Services\SessionGameplay;

use App\Exceptions\DataNonExistingException;

class SessionStorageService
{
    public function gameplaySessionExistenceCheck(Array $gameplaySession)
    {
        if(!$gameplaySession || empty($gameplaySession)) {
            
            throw new DataNonExistingException(
                'The gameplay session was lost somewhere, 
                sorry for inconvenience, we do not know where the data was lost,
                we do not know how the data was lost, but the programmer whom 
                is on the case has a very particular set of skills and he 
                will look for the bug, he will find it... and he will kill you. 
                FIX FIX I meant to say , and he will FIX THE BUG'
            );
        }
    }
}