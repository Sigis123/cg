<?php

namespace App\Services\SessionGameplay;

use App\Repositories\CardRepository;

class HandCardsRandomizerService
{
    /** @var CardRepository **/
    protected $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function getRandomHandCards(Array $deckCardsId, int $cardAmmount)
    {
        if(count($deckCardsId) >= $cardAmmount) {

            $handCardsKeys = array_rand($deckCardsId, $cardAmmount);

            foreach($handCardsKeys as $handCardKey) {

                $handCardsId[] = $deckCardsId[$handCardKey];
            }
        }else {

            $handCardsId = $deckCardsId;
        }

        $differentCards = $this->cardRepository->findByCardsId($handCardsId);

        foreach($handCardsId as $handCardId) {

            foreach($differentCards as $card) {

                if($card->id == $handCardId) {
                    $handCards[] = $card;
                    break;
                }
            }
        }

        return $handCards;
    }
}