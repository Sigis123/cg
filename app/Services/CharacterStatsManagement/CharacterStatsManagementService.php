<?php

namespace App\Services\CharacterStatsManagement;

use App\CharacterMainStats;
use App\CharacterSecondaryStats;
use App\Exceptions\DataNonExistingException;

class CharacterStatsManagementService
{

    private CONST STRENGTH_MULTIPLIER = 1.5;
    private CONST INTELLIGENCE_MULTIPLIER = 1.05;

    public function realStatsArray(
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    ): Array {

        $realStats = [
            'damageDealt' => $this->damageDealt($characterMainStats, $characterSecondaryStats),
            'dangerLevel' => $this->dangerLevel($characterMainStats, $characterSecondaryStats),
            'luck' => $this->luck($characterMainStats, $characterSecondaryStats),
            'armor' => $this->armor($characterMainStats, $characterSecondaryStats)
        ];
        
        if(!$realStats || empty($realStats)) {
            throw new DataNonExistingException(
                'Real stats array is not found or empty'
            );
        }
        
        return $realStats;
    }

    public function damageDealt(
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    ): float {
        $strengthMultiplier = $this->strengthMultiplier($characterMainStats);

        $realAttack = 
            $characterSecondaryStats->attack * $strengthMultiplier;

        return round($realAttack);
    }

    public function dangerLevel(
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    ): float {
        $intelligenceMultiplier = 
            $this->intelligenceMultiplier($characterMainStats);

        $realSpeed = 
            $characterSecondaryStats->speed * $intelligenceMultiplier; 
            
        return round($realSpeed);
    }

    public function luck(
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    ): float {
        $intelligenceMultiplier = 
            $this->intelligenceMultiplier($characterMainStats);

        $realSmarts = 
            $characterSecondaryStats->smarts * $intelligenceMultiplier;

        return round($realSmarts);
    }

    public function armor(
        CharacterMainStats $characterMainStats,
        CharacterSecondaryStats $characterSecondaryStats
    ): float {
        $intelligenceMultiplier = 
            $this->intelligenceMultiplier($characterMainStats);

        $realDefense = 
            $this->realStats(
                $characterSecondaryStats->defense,
                $intelligenceMultiplier
            );

        return round($realDefense);
    }

    public function realStats(
        int $secondaryStats,
        float $multiplier
    ): float {

        return $realStats = $secondaryStats * $multiplier;
    }

    public function intelligenceMultiplier(
        CharacterMainStats $characterMainStats
    ): float {

        return $intelligenceMultiplier = 
            $characterMainStats->intelligence * self::INTELLIGENCE_MULTIPLIER;
    }

    public function strengthMultiplier(
        CharacterMainStats $characterMainStats
    ): float {

        return $strengthMultiplier = 
            $characterMainStats->strength * self::STRENGTH_MULTIPLIER;
    }
}