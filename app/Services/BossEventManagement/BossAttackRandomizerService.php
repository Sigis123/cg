<?php

namespace App\Services\BossEventManagement;

use Illuminate\Database\Eloquent\Collection;

class BossAttackRandomizerService
{
    private CONST MINIMAL_CHANCE_PERCENTAGE_VALUE = 1;
    private CONST MAXIMUM_CHANCE_PERCENTAGE_VALUE = 100;

    public function getRandomAttack(Collection $bossAttackPool)
    {
        $randomChance = rand(
            self::MINIMAL_CHANCE_PERCENTAGE_VALUE,
            self::MAXIMUM_CHANCE_PERCENTAGE_VALUE
        );
        
        foreach($bossAttackPool as $bossAttack) {
            
            if(($randomChance >= $bossAttack->minimal_random_value)
                &&
                ($randomChance <= $bossAttack->maximum_random_value)
            ) {

                return $bossAttack;
            } 
        }
    }
}