<?php

namespace App\Services\BossEventManagement;

use App\BossEvent;
use Illuminate\Session\Store;
use App\Factories\BossEventsFactory;

class BossResultsService
{
    private CONST MIN_SKIP_CHANCE_VALUE = 1;
    private CONST MAX_SKIP_CHANCE_VALUE = 200;

    /** @var BossEventsFactory **/
    protected $bossEventsFactory;

    public function __construct(BossEventsFactory $bossEventsFactory)
    {
        $this->bossEventsFactory = $bossEventsFactory;
    }

    public function bossDefenseResult(
        Array $realStats,
        int $chosenBossArmor,
        BossEvent $bossEvent
    ): string {

        if($realStats['damageDealt'] > $chosenBossArmor) {
            $damageToBoss = $realStats['damageDealt'] - $chosenBossArmor;
            $bossEvent->subtractHealth($damageToBoss);

            return $resultBossDefenseString = "You dealt $damageToBoss damage to HP";
        }

        return $resultBossDefenseString =
            "You dealt ".$realStats['damageDealt']." damage to armor";
    }


    public function bossAttackResult(
        Array $realStats,
        Array $gameplaySession,
        string $eventBossName
    )
    {
        $skipChance = rand(
            self::MIN_SKIP_CHANCE_VALUE,
            self::MAX_SKIP_CHANCE_VALUE
        );

        if($realStats['luck'] >= $skipChance) {

            $bossEventClass = $this->bossEventsFactory->returnBossEventClass(
                $eventBossName
            );

            return $resultBossAttackString = $bossEventClass->bossAction(
                $gameplaySession
            );
        } 

        return $resultBossAttackString = "Boss attack missed";
    }
}