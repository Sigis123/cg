<?php

namespace App\Services\BossEventManagement;

use Illuminate\Database\Eloquent\Collection;
use App\CharacterSecondaryStats;

class BossStatsService
{
    public function getArmor(
        Collection $bossEventArmor,
        CharacterSecondaryStats $characterSecondaryStats
    )
    {
        foreach($bossEventArmor as $armor) {
            if($characterSecondaryStats->speed >= $armor->necessary_speed_amount) {
                $armorValues[] =  $armor->armor_amount;
            }
        }

        return min($armorValues);
    }

}