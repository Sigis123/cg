<?php

namespace App\Services\BossEventManagement;

use App\CharacterMainStats;

class DefeatedBossPerksService
{
    private CONST HEALTH_AMOUNT_TO_ADD = 1;
    private CONST SOUL_AMOUNT_TO_ADD = 2;

    public function defaultCharacterStatsPerk(
        CharacterMainStats $characterMainStats
    ): void {

        $characterMainStats->addHealth(self::HEALTH_AMOUNT_TO_ADD);
        $characterMainStats->addSoul(self::SOUL_AMOUNT_TO_ADD);
        
        return;
    }
}