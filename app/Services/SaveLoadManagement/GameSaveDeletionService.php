<?php 

namespace App\Services\SaveLoadManagement;

use App\GameSave;
use App\Repositories\GameSaveRepository;

class GameSaveDeletionService
{
    /** @var GameSaveRepository **/
    private $gameSaveRepository;

    public function __construct(GameSaveRepository $gameSaveRepository)
    {
        $this->gameSaveRepository = $gameSaveRepository;
    }

    public function gameEndingSaveDeletion(int $userId)
    {
        $saveToDelete = $this->gameSaveRepository
            ->getByUserId($userId)
            ->first();
            
        $saveToDelete->delete();
    }
}