<?php

namespace App\Services\EventManagement;

    
use Illuminate\Database\Eloquent\Collection;
use App\Exceptions\DataNonExistingException;
use App\Exceptions\InputDataNonExistingException;

class EventAffectCheckService
{
    public function checkEventAffect(
        Collection $currentEventAffects,
        Array $realStats,
        string $chosenAffectLevel
    )
    {

        foreach($currentEventAffects as $currentEventAffect) {

            if($currentEventAffect->affect_level_description == $chosenAffectLevel) {

                $chosenEventAffect = $currentEventAffect;
                break;
            }
        }

        $eventMainStat = $currentEventAffect->event_main_status_to_destroy_name;
        
        if(!isset($chosenEventAffect)) {

            throw new DataNonExistingException(
                'There is no chosen level affect in DB'
            );
        } elseif($chosenEventAffect->attack 
                 >
                 $realStats['damageDealt'] 
                 &&
                 $chosenEventAffect->event_main_status_to_destroy_value 
                 >
                 $realStats['dangerLevel']) {

                    throw new InputDataNonExistingException(
                        'Insufficient stats for chosen level affect'
                    );
        }
        
        return $chosenEventAffect;
    }
}