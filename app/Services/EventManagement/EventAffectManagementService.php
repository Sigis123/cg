<?php

namespace App\Services\EventManagement;
      
use Illuminate\Database\Eloquent\Collection;

class EventAffectManagementService 
{
    public function selectEventAffect(
        Collection $currentEventAffects,
        Array $realStats
    )
    {
        foreach($currentEventAffects as $currentEventAffect) {
            
            $eventMainStat = $currentEventAffect->event_main_status_to_destroy_name;
            
            if($currentEventAffect->attack 
                >
                $realStats['damageDealt']
                &&
                $currentEventAffect->event_main_status_to_destroy_value 
                >
                $realStats['dangerLevel']) {

                    continue;
            }

            $eventDescription[$currentEventAffect->affect_level_description] =
                $currentEventAffect->affect_description;

            $eventRequirements[$currentEventAffect->affect_level_description] = 
                "Attack of ".$currentEventAffect
                ->attack." or ".$currentEventAffect
                ->event_main_status_to_destroy_name." of ".$currentEventAffect
                ->event_main_status_to_destroy_value; 
        }

        $eventAffects['eventDescription'] = $eventDescription;
        $eventAffects['eventRequirements'] = $eventRequirements;

        return  $eventAffects;
    }
}