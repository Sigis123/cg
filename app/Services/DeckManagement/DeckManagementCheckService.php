<?php

namespace App\Services\DeckManagement;

use App\Services\DeckManagement\DeckManagementService;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use App\Repositories\CardRepository;
use App\Exceptions\CardLimitException;
use App\Exceptions\ObjectDataExistingException;

class DeckManagementCheckService
{
    private CONST NON_EXCLUSIVE_CHARACTERS = 0;
    private CONST MAXIMUM_DECK_AMOUNT = 3;
    private CONST MINIMUM_CARD_LIMIT_IN_DECK = 0;
    private CONST EXTRA_CARD_LIMIT_IN_DECK = 0;

    /** @var CardRepository **/
    protected $cardRepository;

    /** @var CharacterRepository */
    protected $characterRepository;

    /** @var DeckRepository **/
    protected $deckRepository;

    /** @var DeckManagementService */
    protected $deckManagementService;
    
    public function __construct(
        CardRepository $cardRepository,
        CharacterRepository $characterRepository,
        DeckRepository $deckRepository,
        DeckManagementService $deckManagementService
    ) {
        $this->cardRepository = $cardRepository;
        $this->characterRepository = $characterRepository;
        $this->deckRepository = $deckRepository;
        $this->deckManagementService = $deckManagementService;
    }
  
    public function deckCardsAmountCheck(
        Array $selectedCards,
        int $characterId
    ) {
        $permittedCardAmmount = $this
            ->characterRepository
            ->getDeckCardAmmountById($characterId);

        $cardDeckAmmount = $this
            ->deckManagementService
            ->deckCardAmount($selectedCards);

        if($permittedCardAmmount[0]['deck_card_ammount'] <  $cardDeckAmmount) {
            
            $extraCardAmmount = 
                $cardDeckAmmount - $permittedCardAmmount[0]['deck_card_ammount'];

            if($extraCardAmmount > self::EXTRA_CARD_LIMIT_IN_DECK) {

                $extraCardAmmountString = "You have $extraCardAmmount cards, over your deck limit";
                throw new CardLimitException($extraCardAmmountString);

            } elseif($cardDeckAmmount === self::MINIMUM_CARD_LIMIT_IN_DECK) {

                throw new CardLimitException(
                    'You have to choose at least one card'
                );
            }
        }
    }

    public function databaseDecksAmmountCheck(int $characterId, int $userId) 
    {
        $databaseDecks= $this->deckRepository
            ->findByCharacterIdUserId(
                $characterId,
                $userId
        );

        if(count($databaseDecks) >= self::MAXIMUM_DECK_AMOUNT) {

            $deckMessage = "You already have 3 decks with this character";
            
            return $deckMessage; 
        }
    } 
    
    public function deckNameCheck(int $userId, int $characterId, Array $selectedCards) 
    {
        $deckName = $selectedCards['deckName'];
        $deckCheck = $this->deckRepository
            ->getByUserIdCharacterIdAndDeckName(
                $userId, $characterId, $deckName
            );

        if($deckCheck->isNotEmpty()) {

            $deckNameErrorMessage = "This name (".$selectedCards['deckName'].") is already in use";
            throw new ObjectDataExistingException($deckNameErrorMessage);
        }
    }

    public function getDeckCardsId(Array $selectedCards) 
    {
        $deckCardsId = '';
        //dd($selectedCards);
        foreach($selectedCards as $cardId => $card) {

            if(is_int($cardId)) {


                $extraString = str_repeat(
                    ",$cardId",
                    $selectedCards[$card]
                );

                $deckCardsId .= $extraString;    
            }
        }

        return $deckCardsId = trim($deckCardsId, ',');
    }

    public function completeInputedDeckCreationCheck(
        string $characterName,
        int $userId,
        Array $selectedCards,
        int $characterId
    ) {
        $extraCardAmmountString = $this->deckCardsAmountCheck(
            $selectedCards,
            $characterId
        );

        $deckNameErrorMessage = $this->deckNameCheck($userId, $characterId, $selectedCards);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
        $cards = $this->cardRepository->findByCharactersExclusivity([
            $characterId,
            self::NON_EXCLUSIVE_CHARACTERS
        ]);
    }

    public function completeDeckEditionCheck(
        Array $selectedCards,
        int $userId,
        int $characterId,
        string $originalDeckName    
    ) {
        $editedDeckName = $selectedCards['deckName'];
        $existingDeckNames = $this->deckRepository->findNamesWithoutExclusiveNames(
            $userId,
            $characterId,
            [$originalDeckName]
        );

        foreach($existingDeckNames as  $existingDeckName) {

            if($existingDeckName->deck_name == $selectedCards['deckName']) {

                $deckNameErrorMessage = "This name (".$selectedCards['deckName'].") is already in use";
                break;

            }
        }
            
        if(isset($deckNameErrorMessage)) {
            throw new ObjectDataExistingException($deckNameErrorMessage);
        }

        $extraCardAmmountString = $this->deckCardsAmountCheck($selectedCards, $characterId);
        $cards = $this->cardRepository->findByCharactersExclusivity([
            $characterId,
            self::NON_EXCLUSIVE_CHARACTERS
        ]);
    }
}