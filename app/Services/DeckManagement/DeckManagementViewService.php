<?php

namespace App\Services\DeckManagement;

use App\Deck;
use App\Repositories\CardRepository;
use App\Exceptions\InputDataNonExistingException;

class DeckManagementViewService
{

    /** @var CardRepository **/
    protected $cardRepository;

    public function __construct(CardRepository $cardRepository)
    {
        $this->cardRepository = $cardRepository;
    }

    public function editingFormData(Deck $deck, string $deckName)
    {
        $deckCardsAmmount = array_count_values(explode(',', $deck->deck_cards_id));
        $deckCardsId = array_keys($deckCardsAmmount);
        $deckCards = $this->cardRepository->findByCardsId($deckCardsId);
            
        $selectedCards['deckName'] = $deckName;

        foreach($deckCards as $deckCard) {

            $selectedCards[$deckCard->id] = $deckCard->name;
            $selectedCards[$deckCard->name] = $deckCardsAmmount[$deckCard->id];
        }

        if(!$selectedCards || empty($selectedCards)) {
            throw new InputDataNonExistingException(
                "The inputed deck wasn't found"
            );
        }

        return $selectedCards;
    }
}