<?php

namespace App\Services\DeckManagement;

use App\Repositories\CardRepository;
use App\Repositories\DeckRepository;



class DeckManagementService 
{
    private CONST MINIMAL_CARD_AMOUNT_IN_DECK = 0;

    protected $cardRepository;
    protected $deckRepository;

    public function __construct(
        CardRepository $cardRepository,
        DeckRepository $deckRepository
        )
    {
        $this->cardRepository = $cardRepository;
        $this->deckRepository = $deckRepository;
    }


    public function deckCardAmount(Array $selectedCards) 
    {   
        $selectedCardsValue = $this->cardRepository->findCardsByName($selectedCards);
        $cardDeckAmount = self::MINIMAL_CARD_AMOUNT_IN_DECK;

        foreach($selectedCardsValue as $selectedCard) {
            
            //dd($selectedCards);
            //$selectedCardNameFormated = str_replace(' ', '_', $selectedCard->name);
            //dd($selectedCardNameFormated);
            $cardDeckAmount += 
                $selectedCard->value_in_the_deck * $selectedCards[
                    //$selectedCardNameFormated
                    $selectedCard->name
                ];
        }

        return $cardDeckAmount;
    }

    public function getDeckCardsId(Array $selectedCards) {
        $deckCardsId = '';
        foreach($selectedCards as $cardId => $card) {
            if(is_int($cardId)) {
                $extraString = str_repeat(",$cardId", $selectedCards[$card]);
                $deckCardsId .= $extraString;    
            }
        }
        return $deckCardsId = trim($deckCardsId, ',');
    }

    public function getSavedDeckCards(int $userId, int $characterId, string $deckName) 
    {

        $deck = $this->deckRepository->
            findByUserIdCharacterIdAndDeckName(
                $userId,
                $characterId,
                $deckName    
            );
 
        $deckCardsAmmount = array_count_values(
            $this->getDeckCardsIdArray($deck->deck_cards_id
        ));

        $deckCardsId = array_keys($deckCardsAmmount);
        $deckCards = $this->cardRepository->findByCardsId($deckCardsId);
        $selectedCards['deckName'] = $deckName;
        
        foreach($deckCards as $deckCard) {

            $selectedCards[$deckCard->id] = $deckCard->name;
            $selectedCards[$deckCard->name] = $deckCardsAmmount[$deckCard->id];
        }

        return $selectedCards;
    }
    
    public function getDeckCardsIdArray(string $deckIdsArray)
    {
        return explode(',', $deckIdsArray);
    } 

    public function formatPostedSelectedCards($selectedCards)
    {
        foreach($selectedCards as $key => $cards) {

            $selectedCardKeyFormated = str_replace('_', ' ', $key);

            $formattedCardArray["$selectedCardKeyFormated"] = 
                $selectedCards["$key"];

        }

        return $formattedCardArray;        
    }
}