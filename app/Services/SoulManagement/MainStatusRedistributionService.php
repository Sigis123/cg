<?php

namespace App\Services\SoulManagement;

use App\CharacterMainStats;
use App\Exceptions\ActionFailedException;

class MainStatusRedistributionService
{
    public function mainStatusAddition(
        string $statusToAddTo,
        int $amountToAdd,
        CharacterMainStats $characterMainStats
        ) 
    {
        switch($statusToAddTo) {
            case 'health':
                return $characterMainStats->addHealth($amountToAdd);
            case 'strenght':
                return $characterMainStats->addStrength($amountToAdd);    
            case 'momevent':
                return $characterMainStats->addMomevent($amountToAdd);
            case 'intelligence':
                return $characterMainStats->addIntelligence($amountToAdd);
            default:

                $errorMessage = "Redistribution of stats failed, we couldn't 
                add to $statusToAddTo stat";
                throw new ActionFailedException($errorMessage);
        }
    }

    public function mainStatusSubtraction(
        string $statusToTakeFrom,
        int $ammountToSubtract,
        CharacterMainStats $characterMainStats
        )
    {
        switch($statusToTakeFrom) {
            case 'health':
                return $characterMainStats->subtractHealth($ammountToSubtract);
            case 'strenght':
                return $characterMainStats->subtractStrength($ammountToSubtract);    
            case 'momevent':
                return $characterMainStats->subtractMomevent($ammountToSubtract);
            case 'intelligence':
                return $characterMainStats->subtractIntelligence($ammountToSubtract);
            default:

                $errorMessage = "Redistribution of stats failed, we couldn't
                subtract from $statusToTakeFrom stat";
                throw new ActionFailedException($errorMessage);
        }
    }
}