<?php 

namespace App\Services\SoulManagement;

use App\Services\MainStatusRedistribution;
use App\CharacterMainStats;
use App\Exceptions\ActionFailedException;

class SoulStatusRedistributionService
{
    private CONST ADDITION_STAT_AMMOUNT = 1;
    private CONST SUBTRACTION_STAT_AMMOUNT = 1;
    private CONST REQUIRED_SOUL_AMMOUNT_FROM_TO = 1;
    private CONST REQUIRED_SOUL_AMMOUNT_TO = 2;

    /** @var MainStatusRedistributionService **/
    protected $mainStatusRedistributionService;

    public function __construct(
        MainStatusRedistributionService $mainStatusRedistributionService
    ) {
        $this->mainStatusRedistributionService = $mainStatusRedistributionService;
    }

    public function soulStatusRedistributionFromTo(
        CharacterMainStats $characterMainStats,
        string $statusToTakeFrom,
        string $statusToAddTo
    ) {
        if($characterMainStats->soul >= self::REQUIRED_SOUL_AMMOUNT_FROM_TO
            && 
            $statusToTakeFrom 
            && 
            $statusToAddTo
            ) {

            $characterMainStats->subtractSoul(self::REQUIRED_SOUL_AMMOUNT_FROM_TO);
            $this->mainStatusRedistributionService->mainStatusSubtraction(
                $statusToTakeFrom,
                self::ADDITION_STAT_AMMOUNT,
                $characterMainStats
            );

            $this->mainStatusRedistributionService->mainStatusAddition(
                $statusToAddTo,
                self::SUBTRACTION_STAT_AMMOUNT,
                $characterMainStats
            );        

        } else {
            
            $errorMessage = "Redistribtion of stats failed, addition to 
            $statusToAddTo and subtraction from $statusToTakeFrom stats
            unsuccessful";
            throw new ActionFailedException($errorMessage);
        }
    }

    public function soulStatusRedistributionTo(
        CharacterMainStats $characterMainStats,
        string $statusToAddTo
    )
    {
        if($characterMainStats->soul >= self::REQUIRED_SOUL_AMMOUNT_TO 
            && 
            $statusToAddTo
            ) {

            $characterMainStats->subtractSoul(self::REQUIRED_SOUL_AMMOUNT_TO);
            $this->mainStatusRedistributionService->mainStatusAddition(
                $statusToAddTo,
                self::ADDITION_STAT_AMMOUNT,
                $characterMainStats
            );
        } else {

            $errorMessage = "Redistribtion of stats failed, addition to 
            $statusToAddTo stats unsuccessful";
            throw new ActionFailedException($errorMessage);
        }
    }
}

