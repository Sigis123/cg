<?php

namespace App\Events;

use Illuminate\Session\Store;
use App\Repositories\EventRepository;
use App\Exceptions\ModelNotFoundException;
use App\Exceptions\DataNonExistingException;

class CunamusEvent 
{
    protected $gameplaySession;
    protected $affectLevel;
    private CONST EVENT_RED_DAMAGE = 20;
    private CONST EVENT_YELLOW_DAMAGE = 6;
    private CONST EVENT_GREEN_DAMAGE = 1;
    private CONST GOTEN_GOLD_FROM_EVENT = 120;

    public function __construct(Array $gameplaySession, string $affectLevel)
    {
        $this->gameplaySession = $gameplaySession;
        $this->affectLevel = $affectLevel;
    }

    public function eventAction()
    {
        switch($this->affectLevel) {
            case 'White':
                return $this->whiteLevel();
            case 'Green':
                return $this->greenLevel();
            case 'Yellow':
                return $this->yellowLevel();
            case 'Red':
                return $this->redLevel();
            default:
                throw new ModelNotFoundException(
                    "There is no ".$this->affectlevel." event action level"
                );
        }
    }

    public function redLevel()
    {
        return $this->armorCheck(self::EVENT_RED_DAMAGE);

    }
    
    public function yellowLevel()
    {
        return $this->armorCheck(self::EVENT_YELLOW_DAMAGE);
    }
    

    public function greenLevel()
    {
        return $this->armorCheck(self::EVENT_GREEN_DAMAGE);
    }

    public function whiteLevel()
    {
        $character = $this->gameplaySession['character'];

        if(!$character) {

            throw new DataNonExistingException (
                "We couldn't recover ".$this->gameplaySession['character'].
                " character in event level"
            );
        }

        return $character->addGold(self::GOTEN_GOLD_FROM_EVENT);
    }

    public function armorCheck(int $eventDamage)
    {
        $realStats = $this->gameplaySession['realStats'];

        if(!isset($realStats['armor'])) {

            throw new DataNonExistingException (
                "We couldn't recover ".$realStats['armor'].
                " armor for event level check"
            );
        }

        if($realStats['armor'] <= $eventDamage) {

            $characterMainStats = $this->gameplaySession['characterMainStats'];
            $damageToHealth = $eventDamage - $realStats['armor'];

            return $characterMainStats
                ->subtractHealth($damageToHealth);
        }
        return;
    }

}

