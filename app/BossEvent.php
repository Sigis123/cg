<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BossEvent extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'name',
        'description',
        'level',
        'health',
        'skip_attack_random_chance'
    ];

    public function addHealth(int $amount)
    {
        return $this->health += $amount;
    }

    public function subtractHealth(int $amount)
    {
        return $this->health -= $amount;
    }

}
