<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BossEventAttack extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'boss_event_id',
        'attack_name',
        'attack_description',
        'boss_event_affect',
        'random_choice'
    ];
}
