<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Characters\CharacterClass;
use App\Cards\DeckCardClass;
use App\Cards\SteroidsCardClass;
use App\Events\CunamusEventClass;

class TestController extends Controller
{

    public function index() {
        $alpha = [
            'name' => 'alpha',	
            'health' => 5,
            'strenght' => 6,
            'momevent' => 6,
            'intelligence' => 3,
            'attack' => 4,
            'defense' => 4,
            'speed' => 2,
            'smarts' => 3,
            'soul' => 1,
            'item_id' => 'WeakSauce',
            'gold' => 5,
            'deck_card_ammount' => 2
        ];

        $cardDeck = [
        	1 =>'Strike',
            3 => 'Steroids',
            4 =>'Humanity',
            9 => 'Balanced',
            11 => 'DefenseIsTheBestDefense',
            34 =>'JeeroyLenkins',
            5 =>'Shield'
        ];

        $steroids = [
            'name' => 'Strike',
            'status' => 'Offensive',
            'value_in_the_deck' => 1,
            'description' => '+1 strenght(for the rest of the game)',
            'exclusive_character' => 0,
        ];  

        $event = [
            'name' => 'Cunamus',
            'description' => 'Water wave',
            'level' => 1,
            'status' => 1,
            'health' => 8,
            'attack' => 2,
            'event_main_status_to_destroy_name' => 'momevent',
            'event_main_status_to_destroy_value' => 5
        ];



        $character = new CharacterClass($alpha);
        $event = new CunamusEventClass($character, $event);
        $deckCards = new DeckCardClass($character, $cardDeck);
        $steroidsClass = new SteroidsCardClass($character, $steroids); 

        //character => cardAction => character => eventCheck => eventAction => character  


        dd($event->character()->getCharacterName());
        $character->getCharacterStrenght();
        dd($steroidsClass->getNumber());
        dd($steroidsClass->getCardDescription());
        dd($steroidsClass->cardAction());
        dd($steroidsClass->getNumber());
        dd($character->getCharacterStrenght());

        $deckCardsId = $deckCards->getHandRandomCardsId();

        $move = $character->getCharacterMomevent();
        return view('test/test', compact('move', 'cardDeck', 'deckCardsId'));
    }
}
