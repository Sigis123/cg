<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Deck;
use App\Http\Requests\postDeckValidation;
use App\Services\DeckManagement\DeckManagementService;
use App\Services\DeckManagement\DeckManagementCheckService;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use App\Repositories\CardRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Exceptions\DeckLimitException;
use App\Exceptions\ObjectDataExistingException;
use App\Exceptions\CardLimitException;
use App\Services\InputDataExistenceValidation\DeckManagementInputValidationService;
use App\Exceptions\InputDataNonExistingException;

class DeckCreationController extends Controller
{
    /** @var CardRepository **/
    protected $cardRepository;

    /** @var DeckManagementService **/
    protected $deckManagementService;

    /** @var CharacterRepository **/
    protected $characterRepository;

    /** @var DeckRepository **/
    protected $deckRepository;

    /** @var DeckManagementCheckService **/
    protected $deckManagementCheckService;

    /** @var DeckManagementInputValidationService **/
    protected $deckManagementInputValidationService;

    public function __construct(
        CardRepository $cardRepository,
        DeckManagementService $deckManagementService,
        CharacterRepository $characterRepository,
        DeckRepository $deckRepository,
        DeckManagementCheckService $deckManagementCheckService,
        DeckManagementInputValidationService $deckManagementInputValidationService
    ) {
        $this->cardRepository = $cardRepository;
        $this->deckManagementService = $deckManagementService;
        $this->characterRepository = $characterRepository;
        $this->deckRepository = $deckRepository;
        $this->deckManagementCheckService = $deckManagementCheckService;
        $this->deckManagementInputValidationService = $deckManagementInputValidationService;
    }

    public function index($characterName) 
    {
        try {
            $userId = Auth::user()->id;

            $characterId = $this
                ->characterRepository
                ->findByName($characterName)
                ->id;

            $cards = $this
                ->cardRepository
                ->findByCharactersExclusivity([$characterName, 0]);

            $DBDecksAmmountCheck = $this
                ->deckManagementCheckService
                ->databaseDecksAmmountCheck(
                    $characterId,
                    $userId
                );

            if(isset($DBDecksAmmountCheck)) {

                return view('cardManagement/deckCreation/deckOverLimit', [
                    'errorMessage' => $DBDecksAmmountCheck,
                    'characterName' => $characterName
                ]);
            }


            return view('cardManagement/deckCreation/characterDeckCreation', [
                    'characterName' => $characterName,
                    'cards' => $cards
                ]);

        } catch(RepositoryResponseNotFoundException $exception) {

            return view('cardManagement/deckCreation/deckErrorPage',
                ['error' => ($exception->getMessage())] 
            );   
        }catch(\Error $exception) {

            abort(404);
        }
    }

    public function postDeck(Request $request, $characterName) 
    {
        try{
            $userId = Auth::user()->id;

            $deck = $request->except('_token');

            $formattedCardArray = $this
            ->deckManagementService
            ->formatPostedSelectedCards($deck);

            $cards = $this
                ->cardRepository
                ->findByCharactersExclusivity([$characterName, 0]);

            $this->deckManagementInputValidationService
                ->deckNameExistenceValidation($formattedCardArray);
            
            $this->deckManagementInputValidationService
                ->cardExistenceValidation($formattedCardArray);
            
            $characterId = $this->characterRepository->findByName(
                $characterName
            )->id;

            $deckCardsId = $this
                ->deckManagementCheckService
                ->getDeckCardsId($formattedCardArray);

            $completeCheck = $this
                ->deckManagementCheckService
                ->completeInputedDeckCreationCheck(
                    $characterName,
                    $userId,
                    $formattedCardArray,
                    $characterId
            );

            Deck::create([
                'user_id' => $userId,
                'character_id' => $characterId,
                'deck_name' => $formattedCardArray['deckName'],
                'deck_cards_id' => $deckCardsId,
            ]);

            return view('/cardManagement/deckCreation/successfulDeckCreation',
                compact(
                    "deck"
            ));
        } catch (ObjectDataExistingException $exception) {

            $error = $exception->getMessage();

            return view('cardManagement/deckCreation/characterDeckCreation', [
                    'characterName' => $characterName,
                    'cards' => $cards,
                    'deck' => $formattedCardArray,
                    'error' => $error
                ]);
        } catch (CardLimitException $exception) {

            $error = $exception->getMessage();

            return view('cardManagement/deckCreation/characterDeckCreation', [
                    'characterName' => $characterName,
                    'cards' => $cards,
                    'deck' => $formattedCardArray,
                    'error' => $error
            ]);
        } catch (InputDataNonExistingException $exception) {

            $error = $exception->getMessage();

            return view('cardManagement/deckCreation/characterDeckCreation', [
                    'characterName' => $characterName,
                    'cards' => $cards,
                    'deck' => $formattedCardArray,
                    'error' => $error
            ]);
        } catch (RepositoryResponseNotFoundException $exception) {
            
            abort(404, $exception->getMessage());
            //return back()->withError($exception->getMessage())->withInput();
        }catch (\Error $exception) {

            abort(404);
        }
    }
}
