<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DeckManagement\DeckManagementService;
use App\Services\SessionGameplay\HandCardsRandomizerService;
use App\Services\SessionGameplay\SessionDataControlService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Exceptions\DataNonExistingException;

class SoulRedrawManagementController extends Controller
{
    /** @var DeckManagementService **/
    protected $deckManagementService;

    /** @var HandCardsRandomizerService **/
    protected $handCardsRandomizerService;

    /** @var SesionDataControlService **/
    protected $sessionDataControlService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var SessionStorageService **/
    protected $sessionStorageService;


    public function __construct(
        DeckManagementService $deckManagementService,
        HandCardsRandomizerService $handCardsRandomizerService,
        SessionDataControlService $sessionDataControlService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        SessionStorageService $sessionStorageService
    )
    {
        $this->deckManagementService = $deckManagementService;
        $this->handCardsRandomizerService = $handCardsRandomizerService;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->sessionStorageService = $sessionStorageService;
    }

    public function redrawCurrentHandCards(Request $request)
    {
        
        //$playedCardId = $request->input('card');
        /*
        $character = $request->session()->get('character');
        $characterMainStats = $request->session()->get('characterMainStats');
        $characterSecondaryStats = $request->session()->get('characterSecondaryStats');
        $deckCardsId = $request->session()->get('deckCardsId');
        $handCards = $request->session()->get('handCards');
        $currentEvent = $request->session()->get('event');
        $deckName = $request->session()->get('deckName');
        $currentEventAffects = $request->session()->get('eventAffects');
        $realStats = $request->session()->get('realStats');
        $moveTracker = $request->session()->get('moveTracker');
        */
        try {

            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $ammountOfHandCards = count($gameplaySession['handCards']);

            $handCards = $this
                ->handCardsRandomizerService
                ->getRandomHandCards(
                    $gameplaySession['deckCardsId'],
                    $ammountOfHandCards
                );

            $gameplaySession['characterMainStats']->subtractSoul(1);

            $moveTracker = $this->sessionMoveTrackerService->redrawHandTracker(
                $gameplaySession['moveTracker'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats'],
                $gameplaySession['handCards']
            );

            $gameplaySession['handCards'] = $handCards;
            $gameplaySession['moveTracker'] = $moveTracker;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);
            

            /*
            return view('playSession/playSessionGameplay', [
                'handCards' => $handCards,
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'realStats' => $gameplaySession['realStats']
            ]);
            */
            if(isset($gameplaySession['bossEvent'])) { 

                return redirect()->route('redirectBossLoop');
                /*
                $bossEvent = $request->session()->get('bossEvent');
                $chosenBossAttack = $request->session()->get('chosenBossAttack');
                $bossEventArmor = $request->session()->get('bossEventArmor');
                $chosenBossArmor = $request->session()->get('chosenBossArmor');
                
                return view('playSession/bossEventManagement/bossBattle', compact(
                    'handCards',
                    'character',
                    'characterMainStats',
                    'characterSecondaryStats',
                    'deckName',
                    'currentEvent',
                    'ammountOfHandCards',
                    'currentEventAffects',
                    'realStats',
                    'bossEvent',
                    'chosenBossAttack',
                    'bossEventArmor',
                    'chosenBossArmor'
                ));
                */
            }

            return redirect()->route('redirectEventLoop');
            
        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }

    public function redrawWholeHandCards(Request $request)
    {
        /*
        $character = $request->session()->get('character');
        $characterMainStats = $request->session()->get('characterMainStats');
        $characterSecondaryStats = $request->session()->get('characterSecondaryStats');
        $deckCardsId = $request->session()->get('deckCardsId');
        $handCards = $request->session()->get('handCards');
        $currentEvent = $request->session()->get('event');
        $deckName = $request->session()->get('deckName');
        $currentEventAffects = $request->session()->get('eventAffects');
        $realStats = $request->session()->get('realStats');
        $moveTracker = $request->session()->get('moveTracker');
        */
        try {

            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $ammountOfHandCards = $gameplaySession['characterMainStats']
                ->momevent;

            $handCards = $this
                ->handCardsRandomizerService
                ->getRandomHandCards(
                    $gameplaySession['deckCardsId'],
                    $ammountOfHandCards
                );
                
            $gameplaySession['characterMainStats']->subtractSoul(2);


            $moveTracker = $this->sessionMoveTrackerService->redrawHandTracker(
                $gameplaySession['moveTracker'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats'],
                $gameplaySession['handCards']
            );

            $gameplaySession['handCards'] = $handCards;
            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);


            return view('playSession/playSessionGameplay', [
                'handCards' => $handCards,
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'realStats' => $gameplaySession['realStats']
            ]);
            
            if(isset($gameplaySession['bossEvent'])) {

                return redirect()->route('redirectBossLoop');

                /*
                $bossEvent = $request->session()->get('bossEvent');
                $chosenBossAttack = $request->session()->get('chosenBossAttack');
                $bossEventArmor = $request->session()->get('bossEventArmor');
                $chosenBossArmor = $request->session()->get('chosenBossArmor');

                return view('playSession/bossEventManagement/bossBattle', compact(
                    'handCards',
                    'character',
                    'characterMainStats',
                    'characterSecondaryStats',
                    'deckName',
                    'currentEvent',
                    'ammountOfHandCards',
                    'currentEventAffects',
                    'realStats',
                    'bossEvent',
                    'chosenBossAttack',
                    'bossEventArmor',
                    'chosenBossArmor'
                ));
                */
            }

            return redirect()->route('redirectEventLoop');

        } catch(DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }
}
