<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\CunamusEvent;
use App\Repositories\EventAffectRepository;
use App\Services\EventManagement\EventAffectCheckService;
use App\Services\SessionGameplay\SessionDataControlService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Factories\EventsFactory;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\DataNonExistingException;
use App\Exceptions\InputDataNonExistingException;
use App\Exceptions\ClassNonExistingException;
use App\Exceptions\ModelNotFoundException;
use App\Services\SessionGameplay\GameplayEndService;
use Illuminate\Support\Facades\Auth;

class EndStageController extends Controller
{
    /** @var EventAffectRepository **/
    protected $eventAffectRepository;

    /** @var EventAffectCheckService **/
    protected $eventAffectCheckService;

    /** @var SessionDataControlService **/
    protected $sessionDataControlService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var EventsFactory **/
    protected $eventsFactory;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var GameplayEndService **/
    protected $gameplayEndService;

    public function __construct(
        EventAffectRepository $eventAffectRepository,
        EventAffectCheckService $eventAffectCheckService,
        SessionDataControlService $sessionDataControlService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        EventsFactory $eventsFactory,
        SessionStorageService $sessionStorageService,
        GameplayEndService $gameplayEndService
    ) {
        $this->eventAffectRepository = $eventAffectRepository;
        $this->eventAffectCheckService = $eventAffectCheckService;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->eventsFactory = $eventsFactory;
        $this->sessionStorageService = $sessionStorageService;
        $this->gameplayEndService = $gameplayEndService;
    }    

    public function resultStage(Request $request) 
    {    
        try {
            $userId = Auth::user()->id;

            $validatePlayedCardId = $request->validate([
            'affectLevel' => 'required'
            ]);

            $chosenAffectLevel = $request->input('affectLevel');
            
            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $chosenEventAffect = $this->eventAffectCheckService->checkEventAffect(
                $gameplaySession['eventAffects'],
                $gameplaySession['realStats'],
                $chosenAffectLevel
            );

            $minimalMainStatValue = min(
                $gameplaySession['characterMainStats']->mainStatsArray()
            );  
            dd(mb_detect_encoding($chosenEventAffect));
            dd($chosenEventAffect);
            $moveTracker = $this->sessionMoveTrackerService->chosenEventTracker(
                $gameplaySession['moveTracker'],
                $chosenEventAffect,
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $gameplaySession['moveTracker'] = $moveTracker;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            $this->gameplayEndService->defeatedByEventCheck(
                $userId,
                $gameplaySession,
                $minimalMainStatValue
            );

            $this->sessionDataControlService->removeEventFromEventPool(
                $gameplaySession['eventsPool'],
                $gameplaySession['event']
            );
            
            return view('playSession/stageEnd/startStageResults', [
                'currentEventNumber' => $gameplaySession['currentEventNumber'],
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats']
            ]);

        } catch (DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(InputDataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(ClassNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(ModelNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }  
    }
}
