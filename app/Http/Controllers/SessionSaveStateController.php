<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\GameSave;
use App\Repositories\GameSaveRepository;    
use App\Exceptions\RepositoryResponseNotFoundException;

class SessionSaveStateController extends Controller
{
    /** @var GameSaveRepository **/
    private $gameSaveRepository;

    public function __construct(GameSaveRepository $gameSaveRepository) 
    {
        $this->gameSaveRepository = $gameSaveRepository;
    }

    public function saveState(Request $request)
    {
        try {
            $userId = Auth::user()->id;
            $gameplaySession = \Session::get('gameplaySession');

            $SerializedGameplaySession = serialize($gameplaySession); 

            $savedGame = $this->gameSaveRepository
                ->getWithDeleteByUserId($userId)
                ->first();
            
            if($savedGame) {

                GameSave::withTrashed()
                    ->where('id', $savedGame->id)
                    ->where('user_id', $userId)
                    ->update([
                        'save_data' => $SerializedGameplaySession,
                        'deleted_at' => Null
                    ]);

                $request->session()->forget('gameplaySession');

                return view('gameSaveLoadState/gameSavedSuccessfully'); 
            }
 
            GameSave::create([
                'user_id' => $userId,
                'save_data' => $SerializedGameplaySession
            ]);

            $request->session()->forget('gameplaySession');

            return view('gameSaveLoadState/gameSavedSuccessfully');

        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }  catch(\Error $exception) {

            abort(404);
        }
    }
}
