<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GameSaveRepository;
use Illuminate\Support\Facades\Auth;
use function Opis\Closure\unserialize;

class SessionLoadStateController extends Controller
{
    /** @var GameSaveRepository **/
    private $gameSaveRepository;

    public function __construct(GameSaveRepository $gameSaveRepository)
    {   
        $this->gameSaveRepository = $gameSaveRepository;
    }

    public function loadGame(Request $request)
    {
        try {
            $userId = Auth::user()->id;
            $savedGame = $this->gameSaveRepository
                ->getByUserId($userId)
                ->first();

            $savedDecotedData = unserialize($savedGame->save_data);

            $request->session()->put([
                'gameplaySession' => $savedDecotedData
            ]);
                
            if(isset($savedDecotedData['bossEvent'])) {
                
                return redirect()->route('redirectBossLoop');
            }

            return redirect()->route('redirectEventLoop');

        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );

        } catch(\Error $exception) {

            abort(404);
        }
    }
}
