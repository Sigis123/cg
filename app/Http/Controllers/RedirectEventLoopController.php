<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectEventLoopController extends Controller
{

    public function redirectEventLoop()
    {
         $gameplaySession = \Session::get( 'gameplaySession' );

        return view('playSession/playSessionGameplay', [
            'handCards' => $gameplaySession['handCards'],
            'character' => $gameplaySession['character'],
            'characterMainStats' => $gameplaySession['characterMainStats'],
            'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
            'deckName' => $gameplaySession['deckName'],
            'currentEvent' => $gameplaySession['event'],
            'currentEventAffects' => $gameplaySession['eventAffects'],
            'ammountOfHandCards' =>  $gameplaySession['ammountOfHandCards'],
            'realStats' => $gameplaySession['realStats']
        ]);       
    }
}
