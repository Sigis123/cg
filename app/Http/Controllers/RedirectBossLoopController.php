<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectBossLoopController extends Controller
{
    public function redirectBossLoop()
    {
        $gameplaySession = \Session::get('gameplaySession');

        return view('playSession/bossEventManagement/bossBattle', [
            'handCards' => $gameplaySession['handCards'],
            'deckName' => $gameplaySession['deckName'],
            'ammountOfHandCards' => $gameplaySession['ammountOfHandCards'],
            'bossEvent' => $gameplaySession['bossEvent'],
            'chosenBossAttack' => $gameplaySession['chosenBossAttack'],
            'chosenBossArmor' => $gameplaySession['chosenBossArmor'],
            'character' => $gameplaySession['character'],
            'characterMainStats' => $gameplaySession['characterMainStats'],
            'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
            'realStats' => $gameplaySession['realStats']
        ]);


    }
}
