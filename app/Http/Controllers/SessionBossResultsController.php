<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SessionGameplay\SessionStorageService;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\BossEventManagement\BossResultsService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Exceptions\DataNonExistingException;
use App\Exceptions\ClassNonExistingException;
use App\Exceptions\MethodNonExistingException;
use App\Services\BossEventManagement\DefeatedBossPerksService;
use App\Services\SaveLoadManagement\GameSaveDeletionService;
use Illuminate\Support\Facades\Auth;

class SessionBossResultsController extends Controller
{
    private CONST FINAL_LOOP_NUMBER = 8;
    private CONST DEFEATED_BOSS_HEALTH_AMMOUNT = 0;
    private CONST DEFEATED_USER_HEALTH_AMMOUNT = 0;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    /** @var BossResultsService **/
    protected $bossResultsService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var DefeatedBossPerksService **/
    protected $defeatedBossPerksService;

    /** @var GameSaveDeletionService **/
    protected $gameSaveDeletionService;

    public function __construct(
        SessionStorageService $sessionStorageService,
        CharacterStatsManagementService $characterStatsManagementService,
        BossResultsService $bossResultsService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        DefeatedBossPerksService $defeatedBossPerksService,
        GameSaveDeletionService $gameSaveDeletionService
    ) {
        $this->sessionStorageService = $sessionStorageService;
        $this->characterStatsManagementService = $characterStatsManagementService;
        $this->bossResultsService = $bossResultsService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->defeatedBossPerksService = $defeatedBossPerksService;
        $this->gameSaveDeletionService = $gameSaveDeletionService;
    }

    public function encouterResults(Request $request)
    {
        try{
            $userId = Auth::user()->id;
            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);           

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $resultBossDefenseString = $this->bossResultsService->bossDefenseResult(
                $realStats,
                $gameplaySession['chosenBossArmor'],
                $gameplaySession['bossEvent']
            );

            //iskel kas apacioj gal i kiclass metoda 
            if($gameplaySession['bossEvent']->health <= self::DEFEATED_BOSS_HEALTH_AMMOUNT
                && 
                $gameplaySession['currentEventNumber'] !== self::FINAL_LOOP_NUMBER
            ) {

                $bossEventName = $gameplaySession['bossEvent']->name;

                unset(
                    $gameplaySession['bossEvent'],
                    $gameplaySession['bossEventArmor'],
                    $gameplaySession['chosenBossAttack'],
                    $gameplaySession['chosenBossArmor']
                );

                $request->session()->put([
                    'gameplaySession' => $gameplaySession
                ]);

                return redirect()->action('SessionBossResultsController@victoryResults',
                [
                    'bossEventName' => $bossEventName
                ]);

            } elseif($gameplaySession['bossEvent']->health <= self::DEFEATED_BOSS_HEALTH_AMMOUNT
                    && 
                    $gameplaySession['currentEventNumber'] == self::FINAL_LOOP_NUMBER
            ) {
                $this->gameSaveDeletionService->gameEndingSaveDeletion($userId);

                return view('gameEndings/victory');
            }

 
            $resultBossAttackString = $this->bossResultsService->bossAttackResult(
                $realStats,
                $gameplaySession,
                $gameplaySession['bossEvent']->name
            );

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );       

            $minimalMainStatValue = min(
                $gameplaySession['characterMainStats']->mainStatsArray()
            );

            if($minimalMainStatValue <= self::DEFEATED_USER_HEALTH_AMMOUNT) {

                $gameplaySession['currentEvent'] = $gameplaySession['bossEvent'];

                $this->gameSaveDeletionService->gameEndingSaveDeletion($userId);

                return view('gameEndings/defeat', [
                    'character' => $gameplaySession['character'],
                    'characterMainStats' => $gameplaySession['characterMainStats'],
                    'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                    'currentEvent' => $gameplaySession['bossEvent']
                ]);
            }

            return view('playSession/bossEventManagement/bossResults', [
                'realStats' => $realStats,
                'bossEvent' => $gameplaySession['bossEvent'],
                'bossEventArmor' => $gameplaySession['bossEventArmor'],
                'chosenBossAttack' => $gameplaySession['chosenBossAttack'],
                'resultBossAttackString' => $resultBossAttackString,
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'resultBossDefenseString' => $resultBossDefenseString
            ]);

        } catch(DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(ClassNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(MethodNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(\Error $exception) {
            
            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }    
    }

    public function victoryResults(Request $request)
    {
        try{
            $bossEventName = $request->query('bossEventName');

            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService->gameplaySessionExistenceCheck(
                $gameplaySession
            );  

            $this->defeatedBossPerksService->defaultCharacterStatsPerk(
                $gameplaySession['characterMainStats']
            );

            $moveTracker = $this->sessionMoveTrackerService->bossWinTracker(
                $gameplaySession['moveTracker'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $gameplaySession['moveTracker'] = $moveTracker;

            $request->session()->put([
                'gameplaySession' => $gameplaySession,
            ]);

            return view('playSession/bossEventManagement/bossVictory', [
                'bossEventName' => $bossEventName
            ]);
            
        } catch(DataNonExistingException $exception) {
        
            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }

}
