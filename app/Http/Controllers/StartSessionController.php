<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\RepositoryResponseNotFoundException;

class StartSessionController extends Controller
{
    /** @var CharacterRepository **/
    protected $characterRepository;

    /** @var DeckRepository **/
    protected $deckRepository;

    public function __construct(
        CharacterRepository $characterRepository,
        DeckRepository $deckRepository)
    {
        $this->characterRepository = $characterRepository;
        $this->deckRepository = $deckRepository;
    }


    public function characterSelect()
    {
        try {
            $userId = Auth::user()->id;

            $charactersId = $this->deckRepository->findCharacterIdByUserId(
                $userId
            );

            $characterNames = $this->characterRepository->findNameById(
                $charactersId
            );

            $characterNameAmount = count($characterNames);

            return view('playSession/playSessionCharacterSelect', [
                'characterNames' => $characterNames,
                'characterNameAmount' => $characterNameAmount
            ]);
        } catch (RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );  
        }
    }
    
    public function deckSelect($characterName)
    {   
        try {
            $userId = Auth::user()->id;

            $character = $this->characterRepository->findByName($characterName);

            $decks = $this->deckRepository->findByCharacterIdUserId(
                $character->id,
                $userId
            );

            foreach($decks as $deck) {

                $decksName[] = $deck->deck_name;
            }
    
            return view('playSession/playSessionDeckSelect', [
                'decksName' => $decksName,
                'characterName' => $characterName
            ]);
        } catch (RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        }
    }
}
