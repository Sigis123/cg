<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DeckManagement\DeckManagementService;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use App\Repositories\CharacterMainStatsRepository;
use App\Repositories\CharacterSecondaryStatsRepository;
use App\Repositories\EventRepository;
use App\Repositories\EventAffectRepository;
use Illuminate\Support\Facades\Auth;
use App\Services\SessionGameplay\HandCardsRandomizerService;
use App\Services\SessionGameplay\SessionDataControlService;
use App\Services\SessionGameplay\EventRandomizerService;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Exceptions\DataNonExistingException;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Exceptions\InputDataNonExistingException;

class StartStageController extends Controller
{
    private CONST FIRST_EVENT_NUMBER = 1;

    /** @var CharacterRepository **/
    protected $characterRepository;

    /** @var CharacterMainStatsRepository **/
    protected $characterMainStatsRepository;

    /** @var CharacterSecondaryStatsRepository **/
    protected $characterSecondaryStatsRepository;

    /** @var DeckRepository **/
    protected $deckRepository;

    /** @var EventRepository **/
    protected $eventRepository;

    /** @var EventAffectRepository **/
    protected $eventAffectRepository;

    /** @var DeckManagementService **/
    protected $deckManagementService;

    /** @var HandCardsRandomizerService **/
    protected $handCardsRandomizerService;

    /** @var SessionDataControlService **/
    protected $sessionDataControlService;
    
    /** @var EventRandomizerService **/
    protected $eventRandomizerService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    public function __construct(
        CharacterRepository $characterRepository,
        CharacterMainStatsRepository $characterMainStatsRepository,
        CharacterSecondaryStatsRepository $characterSecondaryStatsRepository,
        DeckRepository $deckRepository,
        EventRepository $eventRepository,
        EventAffectRepository $eventAffectRepository,
        DeckManagementService $deckManagementService,
        HandCardsRandomizerService $handCardsRandomizerService,
        SessionDataControlService $sessionDataControlService,
        EventRandomizerService $eventRandomizerService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        CharacterStatsManagementService $characterStatsManagementService
    ) {

        $this->characterRepository = $characterRepository;
        $this->characterMainStatsRepository = $characterMainStatsRepository;
        $this->characterSecondaryStatsRepository = $characterSecondaryStatsRepository;
        $this->deckRepository = $deckRepository;
        $this->eventRepository = $eventRepository;
        $this->eventAffectRepository = $eventAffectRepository;
        $this->deckManagementService = $deckManagementService;
        $this->handCardsRandomizerService = $handCardsRandomizerService;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->eventRandomizerService = $eventRandomizerService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->characterStatsManagementService = $characterStatsManagementService;
    }

    public function sessionGameplay(Request $request, $characterName, $deckName) 
    {
        try{    
            $userId = Auth::user()->id;
            
            $character =  $this->characterRepository->findByName(
                $characterName
            );

            $characterMainStats = $this
                ->characterMainStatsRepository
                ->findByCharacterId($character->id);

            $characterSecondaryStats = $this
                ->characterSecondaryStatsRepository
                ->findByCharacterId($character->id);
            
            $realStats = $this->characterStatsManagementService->realStatsArray(
                $characterMainStats,
                $characterSecondaryStats
            );
            
            $deck = $this->deckRepository->findByUserIdCharacterIdDeckName(
                $userId,
                $character->id,
                $deckName
            );
                                                                                                                                  
            $currentEventNumber = self::FIRST_EVENT_NUMBER;
            
            $levelOfEvent = $this->eventRandomizerService->levelOfEvents(
                $currentEventNumber
            );

            $events = $this->eventRepository->findByEventLevel($levelOfEvent);

            $currentEvent = $this->eventRandomizerService->eventRandomizer(
                $events
            );

            $this->sessionDataControlService->removeEventFromEventPool(
                $events,
                $currentEvent
            );

            $currentEventAffects = $this
                ->eventAffectRepository
                ->findByEventId($currentEvent->id);
            
            $deckIdString = $deck->deck_cards_id;
            
            $deckCardsId = $this->deckManagementService->getDeckCardsIdArray(
                $deckIdString
            );

            $handCards = $this->handCardsRandomizerService->getRandomHandCards(
                $deckCardsId,
                $characterMainStats->momevent
            );

            $ammountOfHandCards = count($handCards);

            $moveTracker = $this->sessionMoveTrackerService->gameplayStartTracker(
                $characterName,
                $deckName,
                $currentEvent,
                $character,
                $characterMainStats,
                $characterSecondaryStats,
                $deckIdString,
                $handCards
            );

            $gameplaySession = [
                'deckName' => $deckName,
                'characterName' => $characterName,
                'event' => $currentEvent,
                'eventAffects' => $currentEventAffects,
                'deckCardsId' => $deckCardsId,
                'handCards' => $handCards,
                'character' => $character,
                'characterMainStats' => $characterMainStats,
                'characterSecondaryStats' => $characterSecondaryStats,
                'currentEventNumber' => $currentEventNumber,
                'levelOfEvent' => $levelOfEvent,
                'eventsPool' => $events,
                'realStats' => $realStats,
                'moveTracker' => $moveTracker,
                'ammountOfHandCards' => $ammountOfHandCards
            ];

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            return redirect()->route('redirectEventLoop');

        } catch (DataNonExistingException $exception) {
            
            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }
}
