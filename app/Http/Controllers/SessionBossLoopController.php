<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SessionGameplay\SessionStorageService;
use App\Services\BossEventManagement\BossAttackRandomizerService;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SessionGameplay\HandCardsRandomizerService;
use App\Services\BossEventManagement\BossStatsService;
use App\Services\SessionGameplay\SessionMoveTrackerService;

class SessionBossLoopController extends Controller
{

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var BossAttackRandomizerService **/
    protected $bossAttackRandomizerService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    /** @var HandCardsRandomizerService **/
    protected $handCardsRandomizerService;

    /** @var BossStatsService **/
    protected $bossStatsService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;


    public function __construct(
        SessionStorageService $sessionStorageService,
        BossAttackRandomizerService $bossAttackRandomizerService,
        CharacterStatsManagementService $characterStatsManagementService,
        HandCardsRandomizerService $handCardsRandomizerService,
        BossStatsService $bossStatsService,
        SessionMoveTrackerService $sessionMoveTrackerService
    )
    {
        $this->sessionStorageService = $sessionStorageService;
        $this->bossAttackRandomizerService = $bossAttackRandomizerService;
        $this->characterStatsManagementService = $characterStatsManagementService;
        $this->handCardsRandomizerService = $handCardsRandomizerService;
        $this->bossStatsService = $bossStatsService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
    }

    public function bossView(Request $request)
    {
        /*
        $bossEvent = $request->session()->get('bossEvent');
        $bossAttackPool = $request->session()->get('bossAttackPool');
        $bossEventArmor = $request->session()->get('bossEventArmor');
        $characterMainStats = $request->session()->get('characterMainStats');
        $characterSecondaryStats = $request->session()->get('characterSecondaryStats');
        $deckCardsId = $request->session()->get('deckCardsId');
        $character = $request->session()->get('character');
        $deckName = $request->session()->get('deckName');
        $moveTracker = $request->session()->get('moveTracker');
        */
        try{

            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);      

            $chosenBossAttack = $this
                ->bossAttackRandomizerService
                ->getRandomAttack($gameplaySession['bossAttackPool']);

            $handCards = $this->handCardsRandomizerService->getRandomHandCards(
                $gameplaySession['deckCardsId'],
                $gameplaySession['characterMainStats']->momevent
            );

            $ammountOfHandCards = count($handCards);

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $chosenBossArmor = $this->bossStatsService->getArmor(
                $gameplaySession['bossEventArmor'],
                $gameplaySession['characterSecondaryStats']
            );

            $moveTracker = $this->sessionMoveTrackerService->bossEventTracker(
                $gameplaySession['moveTracker'],
                $gameplaySession['bossEvent'],
                $chosenBossAttack,
                $gameplaySession['deckCardsId'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $gampelaySession['chosenBossArmor'] = $chosenBossArmor;
            $gampelaySession['chosenBossAttack'] = $chosenBossAttack;
            $gameplaySession['moveTracker'] = $moveTracker;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            return view('playSession/bossEventManagement/bossBattle', [
                'handCards' => $handCards,
                'deckName' => $gameplaySession['deckName'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'bossEvent' => $gameplaySession['bossEvent'],
                'chosenBossAttack' => $gameplaySession['chosenBossAttack'],
                'chosenBossArmor' => $chosenBossArmor,
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'realStats' => $realStats
            ]);
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }
}
