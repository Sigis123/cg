<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\EventAffectRepository;
use App\Services\EventManagement\EventAffectManagementService;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\DataNonExistingException;

class MiddleStageController extends Controller
{
    /** @var EventAffectRepository **/
    protected $eventAffectRepository;

    /** @var EventAffectManagementService **/
    protected $eventAffectManagementService;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    public function __construct(
        EventAffectRepository $eventAffectRepository,
        EventAffectManagementService $eventAffectManagementService,
        SessionStorageService $sessionStorageService
    )
    {
        $this->eventAffectRepository = $eventAffectRepository;
        $this->eventAffectManagementService = $eventAffectManagementService;
        $this->sessionStorageService = $sessionStorageService;
    }

    public function eventAffectChoise(Request $request)
    {
        try{
        
        $gameplaySession = $request->session()->get('gameplaySession');

        $this->sessionStorageService
            ->gameplaySessionExistenceCheck($gameplaySession);

        $currentEventAffects = $this
            ->eventAffectRepository
            ->findByEventId($gameplaySession['event']->id);
        
        $eventAffect = $this->eventAffectManagementService->selectEventAffect(
            $currentEventAffects,
            $gameplaySession['realStats']);

        $gameplaySession['eventAffects'] = $currentEventAffects;
        $gameplaySession['eventDescription'] = $eventAffect['eventDescription'];
        $gameplaySession['eventRequirements'] = $eventAffect['eventRequirements'];

        $request->session()->put([
            'gameplaySession' => $gameplaySession
        ]);

        return view('playSession/eventManagement/eventAffectSelection', [
            'eventAffect' => $eventAffect
        ]);         
            
        } catch (RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch (DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }  
    }
}
