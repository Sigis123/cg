<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\ActionFailedException;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SoulManagement\SoulStatusRedistributionService;
use App\Exceptions\DataNonExistingException;

class SoulStatusRedistributionManagementController extends Controller
{
    /** @var SessionMoveTrackerService */
    protected $sessionMoveTrackerService;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var SoulStatusRedistributionService **/
    protected $soulStatusRedistributionService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    public function __construct(
        SessionMoveTrackerService $sessionMoveTrackerService,
        SessionStorageService $sessionStorageService,
        SoulStatusRedistributionService $soulStatusRedistributionService,
        CharacterStatsManagementService $characterStatsManagementService
    )
    {
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;  
        $this->sessionStorageService = $sessionStorageService;  
        $this->soulStatusRedistributionService = $soulStatusRedistributionService;
        $this->characterStatsManagementService = $characterStatsManagementService;
    }

    public function redistributeStatusFromView(Request $request) 
    {
        $gameplaySession = $request->session()->get('gameplaySession');

        $this->sessionStorageService
            ->gameplaySessionExistenceCheck($gameplaySession);      

        return view('playSession/soulManagement/redistributionFromMainStatus', [
            'characterMainStats' => $gameplaySession['characterMainStats']
        ]);
    }

    public function redistributeStatusFrom(Request $request)
    {
        $validatePlayedCardId = $request->validate([
            'mainStatus' => 'required'
        ]);

        $statusToTakeFrom = $request->input('mainStatus');
        $gameplaySession = $request->session()->get('gameplaySession');

        $this->sessionStorageService
            ->gameplaySessionExistenceCheck($gameplaySession);

        $gameplaySession['statusToTakeFrom'] = $statusToTakeFrom;

        $request->session()->put([
            'gameplaySession' => $gameplaySession
        ]);
        
        return view('playSession/soulManagement/redistributionToMainStatus', [
            'characterMainStats' => $gameplaySession['characterMainStats']
        ]);
    }

    public function redistributeStatusTo(Request $request)
    {
        /*
        $character = $request->session()->get('character');
        $characterMainStats = $request->session()->get('characterMainStats');
        $characterSecondaryStats = $request->session()->get('characterSecondaryStats');
        $deckCardsId = $request->session()->get('deckCardsId');
        $handCards = $request->session()->get('handCards');
        $currentEvent = $request->session()->get('event');
        $deckName = $request->session()->get('deckName');
        $statusToTakeFrom = $request->session()->get('statusToTakeFrom');
        $statusToAddTo = $request->input('mainStatus');
        $ammountOfHandCards = count($handCards);
        $currentEventAffects = $request->session()->get('eventAffects');
        $realStats = $request->session()->get('realStats');
        $moveTracker = $request->session()->get('moveTracker');
        */
        try{

            $validatePlayedCardId = $request->validate([
                'mainStatus' => 'required'
            ]);

            $statusToAddTo = $request->input('mainStatus');
            $gameplaySession = $request->session()->get('gameplaySession');

            $ammountOfHandCards = count($gameplaySession['handCards']);

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $statusToTakeFrom = $gameplaySession['statusToTakeFrom'];
/*
            if($characterMainStats->soul >= 1 && $statusToTakeFrom && $statusToAddTo) {

                $characterMainStats->subtractSoul(1);
                $this->mainStatusRedistributionService->mainStatusSubtraction(
                    $statusToTakeFrom,
                    1,
                    $characterMainStats
                );
                $this->mainStatusRedistributionService->mainStatusAddition(
                    $statusToAddTo,
                    1,
                    $characterMainStats
                );        

            } else {
                $errorMessage = "Something went wrong with redistribution of stats";
                throw new \Exception($errorMessage);
            }
*/

            $this->soulStatusRedistributionService->soulStatusRedistributionFromTo(
                $gameplaySession['characterMainStats'],
                $statusToTakeFrom,
                $statusToAddTo
            );

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $moveTracker = $this->sessionMoveTrackerService->statusRedistributionTracker
            (
                $gameplaySession['moveTracker'],
                $gameplaySession['characterMainStats'],
                $realStats
            );

            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;
            $gameplaySession['realStats'] = $realStats;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            /*
            return view('playSession/playSessionGameplay', [
                'handCards' => $gameplaySession['handCards'],
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'realStats' => $realStats
            ]);
            */
            if(isset($gameplaySession['bossEvent'])) {

                return redirect()->route('redirectBossLoop');
                /*
                $bossEvent = $request->session()->get('bossEvent');
                $chosenBossAttack = $request->session()->get('chosenBossAttack');
                $bossEventArmor = $request->session()->get('bossEventArmor');
                $chosenBossArmor = $request->session()->get('chosenBossArmor');

                return view('playSession/bossEventManagement/bossBattle', compact(
                    'handCards',
                    'character',
                    'characterMainStats',
                    'characterSecondaryStats',
                    'deckName',
                    'currentEvent',
                    'ammountOfHandCards',
                    'currentEventAffects',
                    'realStats',
                    'bossEvent',
                    'chosenBossAttack',
                    'bossEventArmor',
                    'chosenBossArmor'
                ));
                */
            }

            return redirect()->route('redirectEventLoop');

        } catch (ActionFailedException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch (DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
        );
        }    
    }       

    public function statusAdditionView(Request $request) 
    {
        $gameplaySession = $request->session()->get('gameplaySession');

        $this->sessionStorageService
            ->gameplaySessionExistenceCheck($gameplaySession);      

        return view('playSession/soulManagement/mainStatusAddition', [
            'characterMainStats' => $gameplaySession['characterMainStats']
        ]);
    }

    public function statusAddition(Request $request)
    {
        /*
        $character = $request->session()->get('character');
        $characterMainStats = $request->session()->get('characterMainStats');
        $characterSecondaryStats = $request->session()->get('characterSecondaryStats');
        $deckCardsId = $request->session()->get('deckCardsId');
        $handCards = $request->session()->get('handCards');
        $currentEvent = $request->session()->get('event');
        $deckName = $request->session()->get('deckName');
        $statusToAddTo = $request->input('mainStatus');
        $currentEventAffects = $request->session()->get('eventAffects');
        $realStats = $request->session()->get('realStats');
        $moveTracker = $request->session()->get('moveTracker');
        */

        try{

            $validatePlayedCardId = $request->validate([
                'mainStatus' => 'required'
            ]);

            $statusToAddTo = $request->input('mainStatus');
            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);           

            $ammountOfHandCards = count($gameplaySession['handCards']);

/*
            if($characterMainStats->soul >= 2 && $statusToAddTo) {
                $characterMainStats->subtractSoul(2);
                $this->mainStatusRedistributionService->mainStatusAddition(
                    $statusToAddTo,
                    1,
                    $characterMainStats
                );
            } else {
                $errorMessage = "Something went wrong with retrieving stats for addition";
                throw new \Exception($errorMessage);
            }
*/

            $this->soulStatusRedistributionService->soulStatusRedistributionTo(
                $gameplaySession['characterMainStats'],
                $statusToAddTo
            );
            
            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $moveTracker = $this->sessionMoveTrackerService->statusRedistributionTracker
            (
                $gameplaySession['moveTracker'],
                $gameplaySession['characterMainStats'],
                $realStats
            );

            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;
            $gameplaySession['realStats'] = $realStats;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            /*
            return view('playSession/playSessionGameplay', [
                'handCards' => $gameplaySession['handCards'],
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'realStats' => $realStats
            ]);
            */

            if(isset($gameplaySession['bossEvent'])) {
                
                return redirect()->route('redirectBossLoop');

                /*
                $bossEvent = $request->session()->get('bossEvent');
                $chosenBossAttack = $request->session()->get('chosenBossAttack');
                $bossEventArmor = $request->session()->get('bossEventArmor');
                $chosenBossArmor = $request->session()->get('chosenBossArmor');

                return view('playSession/bossEventManagement/bossBattle', compact(
                    'handCards',
                    'character',
                    'characterMainStats',
                    'characterSecondaryStats',
                    'deckName',
                    'currentEvent',
                    'ammountOfHandCards',
                    'currentEventAffects',
                    'realStats',
                    'bossEvent',
                    'chosenBossAttack',
                    'bossEventArmor',
                    'chosenBossArmor'
                ));
                */
            }

            return redirect()->route('redirectEventLoop');

       } catch(DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(ActionFailedException $exception) {

             return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );           
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
        );
        }    
    }
}
