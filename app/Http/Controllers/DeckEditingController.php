<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\DeckManagement\DeckManagementService;
use App\Services\DeckManagement\DeckManagementCheckService;
use App\Repositories\DeckRepository;
use App\Repositories\CharacterRepository;
use App\Repositories\CardRepository;
use Illuminate\Support\Facades\Auth;
use App\Deck;
use App\Services\DeckManagement\DeckManagementViewService;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Exceptions\InputDataNonExistingException;
use App\Exceptions\ObjectDataExistingException;
use App\Exceptions\CardLimitException;

class DeckEditingController extends Controller
{
    private CONST NON_EXCLUSIVE_CHARACTERS = 0;

    /** @var CardRepository **/
    protected $cardRepository;

    /** @var CharacterRepository **/
    protected $characterRepository;

    /** @var DeckRepository **/
    protected $deckRepository;

    /** @var DeckManagementCheckService **/
    protected $deckManagementCheckService;

    /** @var DeckManagementService **/
    protected $deckManagementService;

    /** @var DeckManagementViewService **/
    protected $deckManagementViewService;

    public function __construct(
        CardRepository $cardRepository,
        CharacterRepository $characterRepository,
        DeckRepository $deckRepository,
        DeckManagementCheckService $deckManagementCheckService,
        DeckManagementService $deckManagementService,
        DeckManagementViewService $deckManagementViewService
    ) {
        $this->cardRepository = $cardRepository;
        $this->characterRepository = $characterRepository;
        $this->deckRepository = $deckRepository;
        $this->deckManagementCheckService = $deckManagementCheckService;
        $this->deckManagementService = $deckManagementService;
        $this->deckManagementViewService = $deckManagementViewService;
    }

    public function deckSelect($characterName)
    {
        try {

            $characterId = $this->characterRepository->findByName($characterName)->id;
            $decks = $this->deckRepository->findByCharacterId($characterId);
            $decksAmmount = count($decks);

            return view('/cardManagement/deckEditing/selectDeckEditing', compact(
                'characterName',
                'decks',
                'decksAmmount'
            ));
        } catch (RepositoryResponseNotFoundException $exception) {

            abort(404, $exception->getMessage());
        } catch (\Error $exception) {

            abort(404);
        }


    }

    public function showDeckEditing($characterName, $deckName)
    {
        try{

            $userId = Auth::user()->id;
            $chosenCharacter = $this->characterRepository->findByName($characterName);
            $deck = $this->deckRepository->findByUserIdCharacterIdDeckName(
                $userId,
                $chosenCharacter->id,
                $deckName
            );

            $selectedCards = $this
                ->deckManagementViewService
                ->editingFormData($deck, $deckName);

            $originalDeckName = $selectedCards['deckName'];

            $deckCardAmount = $this
                ->deckManagementService
                ->deckCardAmount($selectedCards);
            
            $characterCardAmount = $chosenCharacter->deck_card_ammount;

            $cards = $this
                ->cardRepository
                ->findByCharactersExclusivity([
                    $characterName,
                    self::NON_EXCLUSIVE_CHARACTERS
                ]);

            return view('/cardManagement/deckEditing/deckEditing', [
                'characterName' => $characterName,
                'cards' => $cards,
                'selectedCards' => $selectedCards,
                'characterCardAmount' => $characterCardAmount,
                'deckCardAmount' => $deckCardAmount,
                'originalDeckName' => $originalDeckName 
            ]);
        } catch(InputDataNonExistingException $exception) {

            return view('cardManagement/deckCreation/deckErrorPage',
                ['error' => ($exception->getMessage())] 
            );   

        } catch(RepositoryResponseNotFoundException $exception)  {
            
            abort(404, $exception->getMessage());
        } catch(\Error $exception) {
        
            abort(404);
        }
    }

    public function deckUpdateEditing(Request $request, $characterName, $deckName) 
    {
        try{

            $userId = Auth::user()->id;
            $chosenCharacter = $this
                ->characterRepository
                ->findByName($characterName);

            $chosenCharacterId = $chosenCharacter->id;
            $characterCardAmmount = $chosenCharacter->deck_card_ammount;
            $originalDeckName = $request->input('originalDeckName');
         
            $deckId = $this->deckRepository->findByUserIdCharacterIdDeckName(
                $userId,
                $chosenCharacterId,
                $originalDeckName
            )->id;

            $selectedCards = $request->except('_token');
                
            $formattedCardArray = $this
                ->deckManagementService
                ->formatPostedSelectedCards($selectedCards);

            foreach($selectedCards as $key => $cards) {

                $selectedCardKeyFormated = str_replace('_', ' ', $key);

                $formattedCardArray["$selectedCardKeyFormated"] = 
                    $selectedCards["$key"];

            }
                


            $deckCardAmmount = $this
                ->deckManagementService
                ->deckCardAmount($formattedCardArray);           

            $cards = $this
                ->cardRepository
                ->findByCharactersExclusivity([
                    $characterName,
                    self::NON_EXCLUSIVE_CHARACTERS
                ]);
          
            $this->deckManagementCheckService->completeDeckEditionCheck(
                $formattedCardArray,
                $userId,
                $chosenCharacterId,
                $originalDeckName
            );

            $deckCardsId = $this->deckManagementCheckService->getDeckCardsId(
                $formattedCardArray
            );

            Deck::where('id',$deckId)->update([
                'deck_name' => $selectedCards['deckName'],
                'deck_cards_id' => $deckCardsId
            ]);

            return view('/cardManagement/deckEditing/successfulDeckEditing', [
                "selectedCards" => $selectedCards
            ]);
        } catch(ObjectDataExistingException $exception) {

            $error = $exception->getMessage();

            return view('/cardManagement/deckEditing/deckEditing', compact(
                'characterName',
                'cards',
                'selectedCards',
                'characterCardAmmount',
                'deckCardAmmount',
                'originalDeckName',
                'error'
            ));
        } catch(CardLimitException $exception) {

            $error = $exception->getMessage();

            return view('/cardManagement/deckEditing/deckEditing', compact(
                'characterName',
                'cards',
                'selectedCards',
                'characterCardAmmount',
                'deckCardAmmount',
                'originalDeckName',
                'error'
            ));
        } catch(RepositoryResponseNotFoundException $exception) {

            abort(404, $exception->getMessage());
        }
    }
}

