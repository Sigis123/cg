<?php   

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\BossEventRepository;
use App\Services\SessionGameplay\EventRandomizerService;
use App\Repositories\BossEventAttackRepository;
use App\Repositories\BossEventArmorRepository;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SessionGameplay\HandCardsRandomizerService;
use App\Services\SessionGameplay\SessionDataControlService;
use App\Services\BossEventManagement\BossAttackRandomizerService;
use App\Services\BossEventManagement\BossStatsService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Factories\BossEventsFactory;
use App\Factories\CardsFactory;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\RepositoryResponseNotFoundException;
use App\Exceptions\DataNonExistingException;
use App\Services\CardManagement\CardClassRetrievementService;
use App\Exceptions\InputDataNonExistingException;

class SessionBossController extends Controller
{
    /** @var BossEventRepository **/
    protected $bossEventRepository;

    /** @var EventRandomizerService **/
    protected $eventRandomizerService;

    /** @var BossEventAttackRepository **/
    protected $bossEventAttackRepository;

    /** @var BossEventArmorRepository **/
    protected $bossEventArmorRepository;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    /** @var HandCardsRandomizerService **/
    protected $handCardsRandomizerService;

    /** @var SessionDataControlService **/
    protected $sessionDataControlService;

    /** @var BossAttackRandomizerService **/
    protected $bossAttackRandomizerService;

    /** @var BossStatsService **/
    protected $bossStatsService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var BossEventsFactory **/
    protected $bossEventsFactory;

    /** @var CardsFactory **/
    protected $cardsFactory;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var CardClassRetrievementService **/
    protected $cardClassRetrievementService;

    public function __construct(
        BossEventRepository $bossEventRepository,
        BossEventAttackRepository $bossEventAttackRepository,
        BossEventArmorRepository $bossEventArmorRepository,
        EventRandomizerService $eventRandomizerService,
        CharacterStatsManagementService $characterStatsManagementService,
        HandCardsRandomizerService $handCardsRandomizerService,
        SessionDataControlService $sessionDataControlService,
        BossAttackRandomizerService $bossAttackRandomizerService,
        BossStatsService $bossStatsService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        BossEventsFactory $bossEventsFactory,
        CardsFactory $cardsFactory,
        SessionStorageService $sessionStorageService,
        CardClassRetrievementService $cardClassRetrievementService
        )
    {
        $this->bossEventRepository = $bossEventRepository;
        $this->bossEventAttackRepository = $bossEventAttackRepository;
        $this->bossEventArmorRepository = $bossEventArmorRepository;
        $this->eventRandomizerService = $eventRandomizerService;
        $this->characterStatsManagementService = $characterStatsManagementService;
        $this->handCardsRandomizerService = $handCardsRandomizerService;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->bossAttackRandomizerService = $bossAttackRandomizerService;
        $this->bossStatsService = $bossStatsService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->bossEventsFactory = $bossEventsFactory;
        $this->cardsFactory = $cardsFactory;
        $this->sessionStorageService = $sessionStorageService;
        $this->cardClassRetrievementService = $cardClassRetrievementService;
    }

    public function bossView(Request $request)
    {
        try{

            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $bossEventPool = $this->bossEventRepository->findByLevel(
                $gameplaySession['levelOfEvent']
            );

            $bossEvent = $this
                ->eventRandomizerService
                ->eventRandomizer($bossEventPool);

            $bossAttackPool = $this
                ->bossEventAttackRepository
                ->findByBossId($bossEvent->id);

            $chosenBossAttack = $this
                ->bossAttackRandomizerService
                ->getRandomAttack($bossAttackPool);

            $bossEventArmor = $this
                ->bossEventArmorRepository
                ->findByBossId($bossEvent->id);

            $handCards = $this->handCardsRandomizerService->getRandomHandCards(
                $gameplaySession['deckCardsId'],
                $gameplaySession['characterMainStats']->momevent
            );

            $ammountOfHandCards = count($handCards);

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );
            
            $chosenBossArmor = $this->bossStatsService->getArmor(
                $bossEventArmor,
                $gameplaySession['characterSecondaryStats']
            );
            
            $moveTracker = $this->sessionMoveTrackerService->bossEventTracker(
                $gameplaySession['moveTracker'],
                $bossEvent,
                $chosenBossAttack,
                $gameplaySession['deckCardsId'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $gameplaySession['bossEvent'] = $bossEvent;
            $gameplaySession['chosenBossAttack'] = $chosenBossAttack;
            $gameplaySession['chosenBossArmor'] = $chosenBossArmor;
            $gameplaySession['bossEventArmor'] = $bossEventArmor;
            $gameplaySession['bossAttackPool'] = $bossAttackPool;
            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['handCards'] = $handCards;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;
            $gameplaySession['realStats'] = $realStats;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            return redirect()->route('redirectBossLoop');

        } catch (RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch (DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }
}
