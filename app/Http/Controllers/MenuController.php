<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Repositories\GameSaveRepository;
use App\Exceptions\RepositoryResponseNotFoundException;

class MenuController extends Controller
{
    /** @var GameSaveRepository **/
    private $gameSaveRepository;

    public function __construct(GameSaveRepository $gameSaveRepository) 
    {
        $this->gameSaveRepository = $gameSaveRepository;
    }

    public function index()
    {
        try {
            $userId = Auth::user()->id;
            $savedGame = $this->gameSaveRepository->getByUserId($userId);
            $savedGameCheck = $savedGame->isNotEmpty();

            return view('/menu/menu', [
              'savedGameCheck' => $savedGameCheck
            ]);

        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );

        }  catch(\Error $exception) {

            abort(404);
        }
    }
}
