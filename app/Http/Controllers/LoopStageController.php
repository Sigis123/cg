<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SessionGameplay\EventRandomizerService;
use App\Services\SessionGameplay\HandCardsRandomizerService;   
use App\Repositories\EventRepository; 
use App\Services\SessionGameplay\SessionDataControlService;
use App\Repositories\CharacterSecondaryStatsRepository;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Services\SessionGameplay\SessionStorageService;
use App\Exceptions\RepositoryResponseNotFoundException;

class LoopStageController extends Controller
{
    protected CONST EVENT_LEVEL_UP = 1;

    /** @var EventRandomizerService **/
    protected $eventRandomizerService;

    /** @var HandCardsRandomizerService **/
    protected $handCardsRandomizerService;

    /** @var EventRepository **/
    protected $eventRepository;

    /** @var SessionDataControlService **/
    protected $sessionDataControlService;

    /** @var CharacterSecondaryStatsRepository **/
    protected $characterSecondaryStatsRepository;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    public function __construct(
        EventRandomizerService $eventRandomizerService,
        HandCardsRandomizerService $handCardsRandomizerService,
        EventRepository $eventRepository,
        SessionDataControlService $sessionDataControlService,
        CharacterSecondaryStatsRepository $characterSecondaryStatsRepository,
        SessionMoveTrackerService $sessionMoveTrackerService,
        SessionStorageService $sessionStorageService,
        CharacterStatsManagementService $characterStatsManagementService
    )
    {
        $this->eventRandomizerService = $eventRandomizerService;
        $this->handCardsRandomizerService = $handCardsRandomizerService;
        $this->eventRepository = $eventRepository;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->characterSecondaryStatsRepository = $characterSecondaryStatsRepository;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->sessionStorageService = $sessionStorageService;
        $this->characterStatsManagementService = $characterStatsManagementService;
    }

    public function startStage(Request $request)
    {
        try {
            
           $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $gameplaySession['currentEventNumber'] += self::EVENT_LEVEL_UP;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);
    
            //$lastLevelOfEvent = $request->session()->get('levelOfEvent');

            $levelOfEvent = $this->eventRandomizerService->levelOfEvents(
                $gameplaySession['currentEventNumber']
            );

            if($gameplaySession['levelOfEvent'] !== $levelOfEvent) {
                
                $gameplaySession['levelOfEvent'] = $levelOfEvent;
                $request->session()->put([
                    'gameplaySession' => $gameplaySession
                ]);
            }
            
            $handCards = $this->handCardsRandomizerService->getRandomHandCards(
                $gameplaySession['deckCardsId'],
                $gameplaySession['characterMainStats']->momevent
            );
            
            $ammountOfHandCards = count($handCards);
            
            $originalCharacterSecondaryStats = $this
                ->characterSecondaryStatsRepository
                ->findByCharacterId($gameplaySession['character']->id);
            /*
            $characterStatsManagementService = new CharacterStatsManagementService(
                $characterMainStats,
                $characterSecondaryStats
            );
            */

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $originalCharacterSecondaryStats
            );

            $moveTracker = $this->sessionMoveTrackerService->gamePlayLoopTracker(
                $gameplaySession['moveTracker'],
                $gameplaySession['deckCardsId'],
                $gameplaySession['event'],
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $originalCharacterSecondaryStats
            );

            $gameplaySession['characterSecondaryStats'] =
                $originalCharacterSecondaryStats;
            $gameplaySession['realStats'] = $realStats;
            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            return view('playSession/playSessionGameplay', [
                'handCards' => $handCards,
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'ammountOfHandCards' => $ammountOfHandCards,
                'realStats' => $realStats           
            ]);

        } catch (DataNonExistingException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch (RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }  
    }
}
