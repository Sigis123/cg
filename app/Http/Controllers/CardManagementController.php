<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Repositories\CharacterRepository;
use App\Repositories\DeckRepository;
use App\Exceptions\RepositoryResponseNotFoundException;

class CardManagementController extends Controller
{
    /** @var CharacterRepository **/
    private $charactersRepository;

    /** @var DeckRepository **/
    private $deckRepository;

    public function __construct(
        CharacterRepository $characterRepository,
        DeckRepository $deckRepository
    ) {
        $this->characterRepository = $characterRepository;
        $this->deckRepository = $deckRepository;
    }

    public function index()
    {
        try{
            
            $userId = Auth::user()->id;
            $characters = $this->characterRepository->getName();

            $editableDeckCharacterId = $this
                ->deckRepository
                ->findCharacterIdByUserId($userId);

            $editableDeckCharacterName = $this
                ->characterRepository
                ->findNameById($editableDeckCharacterId);
            
            return view('/cardManagement/characterCardDecs', [
               'characters' => $characters,
               'editableDeckCharacterName' => $editableDeckCharacterName
            ]);
        } catch(RepositoryResponseNotFoundException $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(\Error $exception) {

            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            );
        }
    }
}
