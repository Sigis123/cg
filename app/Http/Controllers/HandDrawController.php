<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SessionGameplay\SessionStorageService;
use App\Services\CardManagement\CardClassRetrievementService;
use App\Services\SessionGameplay\SessionDataControlService;
use App\Services\CharacterStatsManagement\CharacterStatsManagementService;
use App\Services\SessionGameplay\SessionMoveTrackerService;
use App\Services\BossEventManagement\BossStatsService;
use App\Exceptions\DataNonExistingException;
use App\Exceptions\InputDataNonExistingException;

class HandDrawController extends Controller
{

    /** @var SessionStorageService **/
    protected $sessionStorageService;

    /** @var CardClassRetrievementService **/
    protected $cardClassRetrievementService;

    /** @var SessionDataControlService **/
    protected $sessionDataControlService;

    /** @var CharacterStatsManagementService **/
    protected $characterStatsManagementService;

    /** @var SessionMoveTrackerService **/
    protected $sessionMoveTrackerService;

    /** @var BossStatsService **/
    protected $bossStatsService;

    public function __construct(
        SessionStorageService $sessionStorageService,
        CardClassRetrievementService $cardClassRetrievementService,
        SessionDataControlService $sessionDataControlService,
        CharacterStatsManagementService $characterStatsManagementService,
        SessionMoveTrackerService $sessionMoveTrackerService,
        BossStatsService $bossStatsService
    ) {
        $this->sessionStorageService = $sessionStorageService;
        $this->cardClassRetrievementService = $cardClassRetrievementService;
        $this->sessionDataControlService = $sessionDataControlService;
        $this->characterStatsManagementService = $characterStatsManagementService;
        $this->sessionMoveTrackerService = $sessionMoveTrackerService;
        $this->bossStatsService = $bossStatsService;
    }

    public function sessionGameplayHandLoop(Request $request)
    {
        try{
            
            $validatePlayedCardId = $request->validate([
                'card' => 'required'
            ]);
            
            $playedCardId = $request->input('card');
            $gameplaySession = $request->session()->get('gameplaySession');

            $this->sessionStorageService
                ->gameplaySessionExistenceCheck($gameplaySession);

            $cardClass = $this
                ->cardClassRetrievementService
                ->gettingCardClass($gameplaySession, $playedCardId);

            $cardClass->cardAction();

            $handCards = $this
                ->sessionDataControlService
                ->removeUsedCardFromHand(
                    $gameplaySession['handCards'],
                    $playedCardId
                );

            $deckCardsId = $this
                ->sessionDataControlService
                ->removeUsedCardFromDeck(
                    $gameplaySession['deckCardsId'],
                    $playedCardId
            );

            $ammountOfHandCards = count($handCards);

            $realStats = $this->characterStatsManagementService->realStatsArray(
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats']
            );

            $moveTracker = $this->sessionMoveTrackerService->handLoopTracker(
                $gameplaySession['moveTracker'],
                $playedCardId,
                $gameplaySession['character'],
                $gameplaySession['characterMainStats'],
                $gameplaySession['characterSecondaryStats'],
                $handCards
            );

            $gameplaySession['deckCardsId'] = $deckCardsId;
            $gameplaySession['handCards'] = $handCards;
            $gameplaySession['realStats'] = $realStats;
            $gameplaySession['moveTracker'] = $moveTracker;
            $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;

            if(isset($gameplaySession['bossEvent'])) {

                $chosenBossArmor = $this->bossStatsService->getArmor(
                    $gameplaySession['bossEventArmor'],
                    $gameplaySession['characterSecondaryStats']
                );

                $gameplaySession['chosenBossArmor'] = $chosenBossArmor;
                $gameplaySession['ammountOfHandCards'] = $ammountOfHandCards;

                $request->session()->put([
                    'gameplaySession' => $gameplaySession
                ]);

                return redirect()->route('redirectBossLoop');
                /*
                return view('playSession/bossEventManagement/bossBattle', [
                    'handCards' => $handCards,
                    'deckName' => $gameplaySession['deckName'],
                    'ammountOfHandCards' => $ammountOfHandCards,
                    'bossEvent' => $gameplaySession['bossEvent'],
                    'chosenBossAttack' => $gameplaySession['chosenBossAttack'],
                    'bossEventArmor' => $gameplaySession['bossEventArmor'],
                    'character' => $gameplaySession['character'],
                    'characterMainStats' => $gameplaySession['characterMainStats'],
                    'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                    'realStats' => $realStats,
                    'chosenBossArmor' => $chosenBossArmor
                ]);
                */
            }

            $request->session()->put([
                'gameplaySession' => $gameplaySession
            ]);

            return redirect()->route('redirectEventLoop');
            /*
            return view('playSession/playSessionGameplay', [
                'handCards' => $handCards,
                'character' => $gameplaySession['character'],
                'characterMainStats' => $gameplaySession['characterMainStats'],
                'characterSecondaryStats' => $gameplaySession['characterSecondaryStats'],
                'deckName' => $gameplaySession['deckName'],
                'currentEvent' => $gameplaySession['event'],
                'currentEventAffects' => $gameplaySession['eventAffects'],
                'ammountOfHandCards' =>  $ammountOfHandCards,
                'realStats' => $realStats
            ]);
            */
        } catch(DataNonExistingException $exception) {
            
            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
            ); 
        } catch(InputDataNonExistingException $exception) {
            
            $error = $exception->getMessage();

            return redirect()->back()->with('error', $error); 
        } catch(\Error $exception) {
            
            return view('playSession/errorPage',
                ['error' => ($exception->getMessage())]
        );
        }    
    }
}



