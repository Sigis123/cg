<?php

namespace App\Cards;

use App\Characters\CharacterClass;

class DeckCardClass
{

	private $character;
	private $cardDeck;
	
	
	public function __construct(CharacterClass $characterClass, Array $cardDeck)
	{
		$this->character = $characterClass;
		$this->cardDeck = $cardDeck;
	}
	
	public function getHandRandomCardsId()
	{
        $maxCardHandAmmount = $this->character->getCharacterMomevent();
        $cardDeckAmmount = count($this->cardDeck);
        $secondaryDeckArray = [];
        $handCardId = [];

        if($cardDeckAmmount > $maxCardHandAmmount) {
            foreach($this->cardDeck as $cardKey => $card) {
                $secondaryDeckArray[] = $cardKey;
            }
            shuffle($secondaryDeckArray);
            
            //$handCardId = array_slice($secondDeckArray);

            for($i = 0; $i < $maxCardHandAmmount; $i++) {
                $handCardId[] = $secondaryDeckArray[$i];
            }
        }else {
            $handCardId = array_keys($this->cardDeck);
        } 

        return $handCardId;
    }

}