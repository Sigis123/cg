<?php

namespace App\Cards\SecondaryStatsCards;

use Illuminate\Session\Store;

class StrikeCard
{
    private CONST ADDITIONAL_ATTACK_AMOUNT = 5;

    protected $session;
    
    public function __construct(Array $session)
    {
        $this->session = $session;
    }
    
    public function cardAction() 
    {
        $characterSecondaryStats = $this->session['characterSecondaryStats'];
        
        $characterSecondaryStats->addAttack(self::ADDITIONAL_ATTACK_AMOUNT);
    }
}