<?php

namespace App\Cards\SecondaryStatsCards;

use Illuminate\Session\Store;

class SanicCard
{
    private CONST ADDITIONAL_SPEED_AMOUNT = 5;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterSecondaryStats = $this->session['characterSecondaryStats'];
		
		$characterSecondaryStats->addSpeed(self::ADDITIONAL_SPEED_AMOUNT);
	}
}