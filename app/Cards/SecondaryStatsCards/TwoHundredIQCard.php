<?php

namespace App\Cards\SecondaryStatsCards;

use Illuminate\Session\Store;

class TwoHundredIQCard
{
    private CONST ADDITIONAL_SMARTS_AMOUNT = 5;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterSecondaryStats = $this->session['characterSecondaryStats'];
		
		$characterSecondaryStats->addSmarts(self::ADDITIONAL_SMARTS_AMOUNT);
	}
}