<?php

namespace App\Cards\SecondaryStatsCards;

use Illuminate\Session\Store;

class RoyalGuardCard
{
    private CONST ADDITIONAL_DEFENSE_AMOUNT = 5;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterSecondaryStats = $this->session['characterSecondaryStats'];
		
		$characterSecondaryStats->addDefense(self::ADDITIONAL_DEFENSE_AMOUNT);
	}
}