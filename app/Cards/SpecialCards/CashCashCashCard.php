<?php

namespace App\Cards\SpecialCards;

use Illuminate\Session\Store;

class CashCashCashCard
{
    private CONST ADDITIONAL_MONEY_AMOUNT = 50;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{
		$character = $this->session['character'];
		
		$character->addGold(self::ADDITIONAL_MONEY_AMOUNT);
	}
}