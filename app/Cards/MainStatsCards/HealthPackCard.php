<?php

namespace App\Cards\MainStatsCards;

use Illuminate\Session\Store;

class HealthPackCard
{
    private CONST ADDITIONAL_HEALTH_AMOUNT = 6;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		
		$characterMainStats->addHealth(self::ADDITIONAL_HEALTH_AMOUNT);
	}
}