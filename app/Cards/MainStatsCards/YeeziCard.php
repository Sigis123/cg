<?php

namespace App\Cards\MainStatsCards;

use Illuminate\Session\Store;

class YeeziCard
{
    private CONST ADDITIONAL_MOMEVENT_AMOUNT = 1;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		
		$characterMainStats->addMomevent(self::ADDITIONAL_MOMEVENT_AMOUNT);
	}
}