<?php

namespace App\Cards\MainStatsCards;

use Illuminate\Session\Store;

class HolyBibleCard
{
    private CONST ADDITIONAL_SOUL_AMOUNT = 2;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		
		$characterMainStats->addSoul(self::ADDITIONAL_SOUL_AMOUNT);
	}
}