<?php

namespace App\Cards\MainStatsCards;

use Illuminate\Session\Store;

class NerdCard
{
    private CONST ADDITIONAL_INTELLIGENCE_AMOUNT = 1;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		
		$characterMainStats->addIntelligence(
			self::ADDITIONAL_INTELLIGENCE_AMOUNT
		);
	}
}