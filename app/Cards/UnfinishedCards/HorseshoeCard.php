<?php

namespace App\Cards;

use Illuminate\Session\Store;

class SteroidsCard
{
    private CONST ADDITIONAL_STRENGTH = 1;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		$characterMainStats->addStrength(self::ADDITIONAL_STRENGTH);
	}
}