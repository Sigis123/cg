<?php

namespace App\Cards\MixedStatsCards;

use Illuminate\Session\Store;

class MeBigerBrainCard
{
	private CONST ADDITIONAL_ATTACK_AMOUNT = 9;
	private CONST SUBTRACTED_INTELLIGENCE_AMOUNT = 4;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterMainStats = $this->session['characterMainStats'];
		$characterSecondaryStats = $this->session['characterSecondaryStats'];

		$characterMainStats->subtractIntelligence(
			self::SUBTRACTED_INTELLIGENCE_AMOUNT
		);

		$characterSecondaryStats->addAttack(self::ADDITIONAL_ATTACK_AMOUNT);
	}
}