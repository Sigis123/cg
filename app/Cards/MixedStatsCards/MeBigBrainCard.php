<?php

namespace App\Cards\MixedStatsCards;

use Illuminate\Session\Store;

class MeBigBrainCard
{
	private CONST ADDITIONAL_ATTACK_AMOUNT = 9;
	private CONST SUBTRACTED_SMARTS_AMOUNT = 4;

	protected $session;

	public function __construct(Array $session)
	{
		$this->session = $session;
	}

	public function cardAction()
	{	
		$characterSecondaryStats = $this->session['charcterSecondaryStats'];	

		$characterSecondaryStats->addAttack(self::ADDITIONAL_HEALTH_AMOUNT);
		$characterSecondaryStats->subtractSmarts(self::SUBTRACTED_SMARTS_AMOUNT);
	}
}