<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Deck extends Model
{
    use softDeletes;

    protected $dates = [
        'deleted_at',
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'user_id',
        'character_id',
        'deck_name',
        'deck_cards_id',
    ];

    public function user() {
        $this->belongsTo(User::class);
    }
}
